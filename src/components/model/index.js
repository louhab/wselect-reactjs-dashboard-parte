import React from "react";
import Modal from "react-bootstrap/Modal";
import { EditOffer } from "./editJob";
export const PopUp = (props) => {
	return (
		<Modal
			{...props}
			size="lg"
			aria-labelledby="contained-modal-title-vcenter"
			centered
		>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">
					{/* Modal heading */}
				</Modal.Title>
			</Modal.Header>
			<EditOffer />
			<Modal.Footer>
				<button onClick={props.onHide}>Close</button>
			</Modal.Footer>
		</Modal>
	);
};
