import React from "react";
import Modal from "react-bootstrap/Modal";

export const EditOffer = ({ data }) => {
	return (
		<Modal.Body>
			<h1>Nouvelle offre d'emploi</h1>
			<p className="pxp-text-light">
				Ajoutez un nouveau poste à la liste des postes de votre
				entreprise.
			</p>
		</Modal.Body>
	);
};
