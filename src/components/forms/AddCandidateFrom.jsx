import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import SuccessCandidate from "../SuccessCandidate";
import toast, { Toaster } from "react-hot-toast";
import useQuery from "../../lib/useQuery";
import { useNavigate } from "react-router-dom";
const LEVELS = [
	{
		label: "one",
		value: 1,
	},
	{ label: "two", value: 2 },
	{ label: "three", value: 3 },
	{ label: "four", value: 4 },
	{ label: "five", value: 5 },
	{ label: "six", value: 6 },
	{ label: "seven", value: 7 },
	{ label: "eight", value: 8 },
	{ label: "nine", value: 9 },
	{ label: "ten", value: 10 },
];

const GENDER = [
	{ label: "man", value: "Homme" },
	{ label: "woman", value: "Femme" },
];

const LEVELS_LANG = [
	{
		label: "one",
		value: "Débutant",
	},
	{ label: "four", value: "Moyen" },
	{ label: "seven", value: "Bien" },
	{ label: "ten", value: "Excellent" },
];

const BOOL = [
	{ label: 0, value: "Non" },
	{
		label: 1,
		value: "Oui",
	},
];

export default function AddCandidateFrom() {
	const navigate = useNavigate();
	const query = useQuery();
	const [submited, setSubmited] = useState(false);
	const [isActive, setIsActive] = useState(false);
	const [domaines, setDomaines] = useState([]);
	const [diplomes, setDiplomes] = useState([]);
	const [loading, setLoading] = useState(false);

	const [offre, setOffre] = useState({
		id: null,
		title: null,
	});
	const [form, setFrom] = useState({
		firstName: "",
		lastName: "",
		phone: "",
		diplomasName: "",
		hasDiplomas: "0",
		yearsExp: "one",
		languageLevel: "one",
		email: "",
		gender: "man",
		domaineDactiviteId: "",
		diplomeId: "",
	});

	const [file, setFile] = useState();

	const saveFile = (e) => {
		setFile(e.target.files[0]);
	};

	function handleChange({ target: { name, value } }) {
		setFrom((init) => ({ ...init, [name]: value }));
		if (name === "hasDiplomas") setIsActive(!!Number(value));
	}

	async function handleSubmit(e) {
		try {
			setLoading(true);
			e.preventDefault();
			const formData = new FormData();
			formData.append("cv_file", file);
			for (const key in form) {
				formData.append(key, form[key]);
			}
			formData.append("offerId", Number(offre?.id));
			await instance.post("/candidates", formData);
			setSubmited(true);
			toast.success("Bravo!");
			setTimeout(() => {
				setSubmited(true);
				toast.success("Bravo!");
			}, 500);
		} catch (e) {
			setLoading(false);
			if (
				"response" in e &&
				"data" in e?.response &&
				(e?.response?.status === 409 || e?.response?.status === 401) &&
				e?.response?.data?.success === false
			) {
				const { error } = e?.response?.data;
				toast.error(error.message, {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			} else {
				toast.error("Merci de remplir le formulaire", {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			}
		}
	}

	useEffect(() => {
		if (query.get("ref") && query.get("id")) {
			const fetchOffer = async () => {
				try {
					const {
						data: { results },
					} = await instance.get(`/offers/${query.get("id")}`);
					if (results) {
						setOffre({
							id: results?.id,
							title: results?.title,
						});
					}

					await instance.get(
						`/campaigns/uid/${query.get("ref")}/${query.get("id")}`
					);
				} catch (error) {
					navigate("/candidate", { replace: true });
				}
			};
			fetchOffer();
		} else {
			navigate("/candidate", { replace: true });
		}
	}, [navigate, query]);

	useEffect(() => {
		instance
			.get(`/domaines`)
			.then((res) => {
				setDomaines(res?.data?.results);
			})
			.catch((err) => {
				console.error(err);
			});
		instance
			.get(`/diplomes`)
			.then((res) => {
				setDiplomes(res?.data?.results);
			})
			.catch((err) => {
				console.error(err);
			});
	}, []);

	return submited ? (
		<>
			<SuccessCandidate name={form?.contactName} />
			<Toaster position="bottom-center" reverseOrder={false} />
		</>
	) : (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<div className="row pb-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<h2 className="pxp-section-h2 text-center">
								Formulaire
								<br /> de Pré-candidature <br />{" "}
								<div style={{ color: "#a61818" }}>
									{offre?.title}
								</div>
							</h2>
							<form
								className="mt-4"
								onSubmit={handleSubmit}
								encType="multipart/form-data"
							>
								<div className="mb-3">
									<label
										htmlFor="first-name"
										className="form-label"
									>
										Nom
									</label>
									<input
										name="firstName"
										value={form?.firstName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="first-name"
										placeholder="Entrez Votre Nom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="last-name"
										className="form-label"
									>
										Prénom
									</label>
									<input
										name="lastName"
										value={form?.lastName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="last-name"
										placeholder="Entrez Votre Prénom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Gender
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="gender"
											onChange={handleChange}
											name="gender"
										>
											{GENDER.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="contact-phone"
										className="form-label"
									>
										Numéro de téléphone
									</label>
									<input
										name="phone"
										value={form?.phone}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-phone"
										placeholder="123-456-789"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="contact-email"
										className="form-label"
									>
										Email
									</label>
									<input
										name="email"
										value={form?.email}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-email"
										placeholder="email@exemple.com"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Domaine D'activite
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="domaineDactiviteId"
											onChange={handleChange}
											name="domaineDactiviteId"
											value={form.domaineDactiviteId}
										>
											<option disabled value={""}>
												Domaine D'activite
											</option>
											{domaines.map((el, i) => (
												<option value={el.id} key={i}>
													{el.name}
												</option>
											))}
										</select>
									</div>
								</div>

								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Diplômes
									</label>
									<select
										className="form-select"
										id="diplome"
										onChange={handleChange}
										name="diplomeId"
										value={form.diplomeId}
									>
										<option disabled value={""}>
											Diplômes
										</option>
										{diplomes.map((el, i) => (
											<option value={el.id} key={i}>
												{el.name}
											</option>
										))}
									</select>
								</div>

								<div className="mb-3">
									<label
										htmlFor="diplomas"
										className="form-label"
									>
										Titulaire d'un diplôme de qualification
										?
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="diplomas"
											onChange={handleChange}
											name="hasDiplomas"
										>
											{BOOL.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>

								<div
									className="mb-3"
									style={{
										display: isActive ? "block" : "none",
									}}
								>
									<label
										htmlFor="has-diplomas"
										className="form-label"
									>
										Si oui, lequel ?
									</label>
									<input
										name="diplomasName"
										value={form.diplomasName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="has-diplomas"
										placeholder="Nom de diplome"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Années d’expériences
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="years-exp"
											onChange={handleChange}
											name="yearsExp"
										>
											{LEVELS.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="languageLevel"
										className="form-label"
									>
										Niveau de langue en français
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="languageLevel"
											onChange={handleChange}
											name="languageLevel"
										>
											{LEVELS_LANG.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="languageLevel"
										className="form-label"
									>
										Uploader Votre CV
									</label>
									<div className="pxp-candidate-cover mb-3">
										<input
											type="file"
											id="pxp-candidate-cover-choose-file"
											onChange={saveFile}
											name="cv_file"
										/>
										<label
											htmlFor="pxp-candidate-cover-choose-file"
											className="pxp-cover"
										>
											{/* <span>UPLOAD VOTER CV</span> */}
										</label>
									</div>
								</div>

								<br />
								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
									disabled={loading}
								>
									S'inscrire
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}
