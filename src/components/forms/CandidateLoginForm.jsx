import React, { useState, useContext } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context/Auth";
import jwt_decode from "jwt-decode";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

export default function CandidateSignupForm() {
	const navigate = useNavigate();

	const { setAuth } = useContext(AuthContext);

	const schema = Joi.object({
		email: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		password: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
	});

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});

	async function onSubmit(values) {
		try {
			const res = await instance.post("/users/login", values);
			const {
				data: { results },
			} = res;

			localStorage.setItem("ws_admin_token", results?.accessToken);

			const decoded = jwt_decode(results?.accessToken);
			setAuth(() => ({
				token: results?.accessToken,
				user: results?.user,
				id: decoded?.id,
				type: decoded?.type,
			}));

			toast.success("Tout s'est bien passé.", {
				style: {
					borderRadius: "10px",
					background: "#fff",
					color: "#333",
					fontSize: "14px",
				},
			});

			setTimeout(() => {
				navigate("/", { replace: true });
			}, 850);
		} catch (e) {
			if (
				"response" in e &&
				"data" in e?.response &&
				(e?.response?.status === 409 || e?.response?.status === 401) &&
				e?.response?.data?.success === false
			) {
				const { error } = e?.response?.data;
				toast.error(error.message, {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			} else {
				toast.error("Veuillez remplir le formulaire", {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			}
		}
	}

	return (
		<section
			style={{
				backgroundColor: "var(--pxpMainColorBlueSky)",
				height: "100vh",
			}}
		>
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="row align-items-center pxp-sign-hero-container">
				<div className="pxp-sign-hero-form">
					<div className="row justify-content-center">
						<div className="col-lg-4 col-xl-4 col-xxl-4">
							<h2 className="text-center">Se connecter</h2>
							<form
								className="mt-4"
								onSubmit={handleSubmit(onSubmit)}
							>
								<div className="form-floating mb-3">
									<input
										id="contact-email"
										name="email"
										type="email"
										className="form-control"
										placeholder="email@exemple.com"
										{...register("email")}
										style={{
											border:
												errors["email"] &&
												errors["email"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null,
										}}
									/>
									<label htmlFor="pxp-signin-page-email">
										Email address
									</label>
									<span
										className="fa fa-envelope-o"
										style={{
											color:
												errors["email"] &&
												errors["email"].message
													? "rgba(255, 0, 0, 0.3)"
													: null,
										}}
									></span>
								</div>

								<div className="form-floating mb-3">
									<input
										id="password"
										name="password"
										type="password"
										className="form-control"
										placeholder="Mot de passe"
										{...register("password")}
										style={{
											border:
												errors["password"] &&
												errors["password"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null,
										}}
									/>

									<label htmlFor="password">Password</label>
									<span
										className="fa fa-lock"
										style={{
											color:
												errors["password"] &&
												errors["password"].message
													? "rgba(255, 0, 0, 0.3)"
													: null,
										}}
									></span>
								</div>
								<br />
								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
								>
									Se connecter
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}
