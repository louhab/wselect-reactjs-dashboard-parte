import React from "react";
import { instance } from "../../lib/axios";
import SuccessEmployer from "../SuccessEmployer";
import toast, { Toaster } from "react-hot-toast";
import { TextInput, Textarea } from "../../components/inputDash/index";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

export default function AddEmployerFrom({ query }) {
	const schema = Joi.object({
		companyName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		contactName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		contactPhone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		address: Joi.string().allow(""),
		email: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		remarks: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		website: Joi.string().allow(""),
	});

	const {
		register,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitSuccessful },
		getValues,
		setError,
	} = useForm({
		resolver: joiResolver(schema),
	});

	async function onSubmit(values) {
		try {
			const queryString = query.get("ref")
				? `?ref=${query.get("ref")}`
				: "";
			await instance.post(`/employers${queryString}`, values);
			setTimeout(() => {
				toast.success("Bravo!");
			}, 500);
		} catch (e) {
			if (
				"response" in e &&
				"data" in e?.response &&
				(e?.response?.status === 409 || e?.response?.status === 401) &&
				e?.response?.data?.success === false
			) {
				const { error } = e?.response?.data;
				setError("email", {
					type: "server",
					message: error.message,
				});
			} else {
				toast.error("Something went wrong! 🔥", {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			}
		}
	}

	return isSubmitSuccessful ? (
		<>
			<SuccessEmployer name={getValues()?.contactName} />
			<Toaster position="bottom-center" reverseOrder={false} />
		</>
	) : (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<div className="row pb-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<h2 className="pxp-section-h2 text-center">
								Inscription Employeur
							</h2>
							<form
								className="mt-4"
								onSubmit={handleSubmit(onSubmit)}
							>
								<TextInput
									name="companyName"
									label="Nom de l'Entreprise"
									register={register}
									errors={errors}
								/>

								<TextInput
									name="contactPhone"
									label="Numéro de téléphone"
									register={register}
									errors={errors}
								/>

								<TextInput
									name="email"
									label="Courriel"
									register={register}
									errors={errors}
									type="email"
								/>
								<TextInput
									name="website"
									label="Site web"
									register={register}
									errors={errors}
								/>

								<TextInput
									name="contactName"
									label="Interlocuteur"
									register={register}
									errors={errors}
								/>

								<TextInput
									name="address"
									label="Adresse"
									register={register}
									errors={errors}
								/>

								<Textarea
									name="remarks"
									label="Besoins spécifiques"
									register={register}
									errors={errors}
								/>

								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
									disabled={!isValid || isSubmitting}
								>
									S'inscrire
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}
