import React from "react";
import { Link } from "react-router-dom";
import Under from "../assets/images/under_construction.svg";

export default function UnderConstruction() {
	return (
		<section
			className="mt-100 pxp-no-hero"
			style={{ marginBottom: "130px" }}
		>
			<div className="pxp-container">
				<h2 className="pxp-section-h2 text-center">
					Cette page est en cours de construction!
				</h2>
				<p className="pxp-text-light text-center">
					Veuillez le consulter ultérieurement.
				</p>

				<div className="pxp-404-fig text-center mt-4 mt-lg-5">
					<img src={Under} alt="Done" />
				</div>

				<div className="mt-4 mt-lg-5 text-center">
					<Link to="/" className="btn rounded-pill pxp-section-cta">
						Vers l'Accueil
						<span className="fa fa-angle-right"></span>
					</Link>
				</div>
			</div>
		</section>
	);
}
