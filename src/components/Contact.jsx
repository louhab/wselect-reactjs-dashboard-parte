import React, { useState } from "react";
import { Link } from "react-router-dom";
import { instance } from "../lib/axios";
import toast, { Toaster } from "react-hot-toast";

const initialForm = {
	name: "",
	email: "",
	message: "",
};

export default function Contact() {
	const [form, setForm] = useState(initialForm);

	function handleChange({ target: { name, value } }) {
		setForm((initialState) => ({
			...initialState,
			[name]: value,
		}));
	}

	async function handleSubmit(e) {
		try {
			e.preventDefault();
			await instance.post("/contact", form);

			setTimeout(() => {
				setForm(initialForm);
				toast.success("Bravo!");
			}, 500);
		} catch (e) {
			console.log(e);
			toast.error("Merci de remplir le formulaire", {
				style: {
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
					fontSize: "14px",
				},
			});
		}
	}
	return (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<h2 className="pxp-section-h2 text-center" style={{
					// fontFamily:"barlow"
				}}>
					N’hésitez pas à nous contacter
				</h2>

				<div className="row mt-4 mt-md-5 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
						<Link
							to="#"
							className="pxp-contact-card-1"
							style={{ justifyContent: "unset" }}
						>
							<div className="pxp-contact-card-1-icon-container">
								<div className="pxp-contact-card-1-icon">
									<span className="fa fa-globe"></span>
								</div>
							</div>
							<div className="pxp-contact-card-1-title">
								107-10150 avenue papineau, Québec, Canada
							</div>
						</Link>
					</div>
					<div className="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
						<Link
							to="#"
							className="pxp-contact-card-1"
							style={{ justifyContent: "unset" }}
						>
							<div className="pxp-contact-card-1-icon-container">
								<div className="pxp-contact-card-1-icon">
									<span className="fa fa-phone"></span>
								</div>
							</div>
							<div className="pxp-contact-card-1-title">
								+1 514 385-0202
							</div>
						</Link>
					</div>
					<div className="col-lg-4 col-xxl-3 pxp-contact-card-1-container">
						<Link
							to="#"
							className="pxp-contact-card-1"
							style={{ justifyContent: "unset" }}
						>
							<div className="pxp-contact-card-1-icon-container">
								<div className="pxp-contact-card-1-icon">
									<span className="fa fa-envelope-o"></span>
								</div>
							</div>
							<div className="pxp-contact-card-1-title">
								info@wselect.ca
							</div>
						</Link>
					</div>
				</div>

				<div className="row pb-100 mt-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<h2 className="pxp-section-h2 text-center" style={{
							}}>
								Nous contacter
							</h2>
							<form className="mt-4" onSubmit={handleSubmit}>
								<div className="mb-3">
									<label
										htmlFor="contact-us-name"
										className="form-label"
									>
										Nom
									</label>
									<input
										type="text"
										className="form-control"
										id="contact-us-name"
										placeholder="Entrez votre nom"
										name="name"
										onChange={handleChange}
										value={form?.name}
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="contact-us-email"
										className="form-label"
									>
										Courriel
									</label>
									<input
										type="text"
										className="form-control"
										id="contact-us-email"
										placeholder="email@example.com"
										name="email"
										onChange={handleChange}
										value={form?.email}
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="contact-us-message"
										className="form-label"
									>
										Message
									</label>
									<textarea
										className="form-control"
										id="contact-us-message"
										placeholder="Entrez votre message ici..."
										name="message"
										onChange={handleChange}
										value={form?.message}
									></textarea>
								</div>
								<button
									type="submit"
									className="btn rounded-pill pxp-section-cta d-block"
									style={{
										width: "100%",
									}}
								>
									Envoyer le message
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}
