import React from "react";
import ProcessOne from "../assets/images/how-to-1-min.png";
import ProcessTwo from "../assets/images/how-to-2-min.png";
import ProcessThree from "../assets/images/how-to-3-min.png";
import ProcessFour from "../assets/images/how-to-4-min.png";
import { COLORS } from "../utils";

function How() {
	return (
		<section
			className="pt-100 pb-100"
			style={{ backgroundColor: COLORS.customMainColorLight }}
		>
			<div className="pxp-container">
				<div className="row">
					<h2 className="pxp-section-h2 text-center" style={{
						fontweight: "normal"
						// fontFamily: "barlow !important"
					}}>
						Comment ça marche
					</h2>
					{/* <div className="col-lg-12"> */}
					<div className="d-flex flex-column flex-lg-row justify-content-around how-to-sec pxp-animate-in-top pxp-in mt-4 mt-md-5 pxp-animate-in">
						<div className="how-to col d-flex flex-column align-items-center">
							<img
								src={ProcessOne}
								alt="one"
								style={{ width: "300px" }}
							/>
							<div className="pxp-cities-card-2-name">
								Je m'inscris
							</div>
						</div>
						<div className="how-to col d-flex flex-column align-items-center">
							<span className="how-icon">
								<img
									src={ProcessTwo}
									alt="two"
									style={{ width: "300px" }}
								/>
							</span>
							<div className="pxp-cities-card-2-name">
								Je crée un nouveau compte
							</div>
						</div>
						<div className="how-to col d-flex flex-column align-items-center">
							<span className="how-icon">
								<img
									src={ProcessThree}
									alt="three"
									style={{ width: "300px" }}
								/>
							</span>
							<div className="pxp-cities-card-2-name">
								Je joins mon CV
							</div>
						</div>
						<div className="how-to col d-flex flex-column align-items-center">
							<span className="how-icon">
								<img
									src={ProcessFour}
									alt="four"
									style={{ width: "300px" }}
								/>
							</span>
							<div className="pxp-cities-card-2-name">
								Je choisis l'offre d'emploi
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default How;
