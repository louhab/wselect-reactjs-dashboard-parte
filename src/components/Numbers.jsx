import React from "react";
import Candidature from "./icons/Candidature";
import JobOffers from "./icons/JobOffers";
import Partenaires from "./icons/Partenaires";

export default function Numbers() {
	return (
		<section className="pt-100 pb-100">
			<div className="pxp-container">
				<h2 className="pxp-section-h2 text-center" style={{
					fontweight: "normal !important"

					// fontFamily: "barlow"
				}}>
					World select en chiffres
				</h2>
				<h5 className="pxp-text-light text-center">Nos réalisations</h5>
				<div className="row mt-4 mt-md-5 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-12 col-md-4 col-lg-3 pxp-categories-card-1-container">
						<div className="pxp-categories-card-1">
							<div className="pxp-categories-card-1-icon-container">
								<div className="pxp-categories-card-1-icon">
									<JobOffers
										style={{
											width: "50px",
											height: "50px",
										}}
									/>
								</div>
							</div>
							<div
								className="pxp-categories-card-1-title"
								style={{
									height: "130px",
									display: "flex",
									flexDirection: "column",
								}}
							>
								<h1>
									<strong>1345</strong>
								</h1>
								<br />
								Offres d'emploi en ligne
							</div>
						</div>
					</div>
					<div className="col-12 col-md-4 col-lg-3 pxp-categories-card-1-container">
						<div className="pxp-categories-card-1">
							<div className="pxp-categories-card-1-icon-container">
								<div className="pxp-categories-card-1-icon">
									<Candidature
										style={{
											width: "50px",
											height: "50px",
										}}
									/>
								</div>
							</div>
							<div
								className="pxp-categories-card-1-title"
								style={{
									height: "130px",
									display: "flex",
									flexDirection: "column",
								}}
							>
								<h1>
									<strong>4800</strong>
								</h1>
								<br />
								Candidatures traitées
							</div>
						</div>
					</div>
					<div className="col-12 col-md-4 col-lg-3 pxp-categories-card-1-container">
						<div className="pxp-categories-card-1">
							<div className="pxp-categories-card-1-icon-container">
								<div className="pxp-categories-card-1-icon">
									<Partenaires
										style={{
											width: "50px",
											height: "50px",
										}}
									/>
								</div>
							</div>
							<div
								className="pxp-categories-card-1-title"
								style={{
									height: "130px",
									display: "flex",
									flexDirection: "column",
								}}
							>
								<h1>
									<strong>100</strong>
								</h1>
								<br />
								Partenaires à l’International
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}
