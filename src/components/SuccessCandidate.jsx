import React from "react";
import { Link } from "react-router-dom";
import Done from "../assets/images/done.svg";

export default function SuccessCandidate({ name }) {
	return (
		<section
			className="mt-100 pxp-no-hero"
			style={{ marginBottom: "50px" }}
		>
			<div className="pxp-container">
				<h2 className="pxp-section-h2 text-center">
					Félicitation {name}
				</h2>
				<p className="pxp-text-light text-center">
					L'équipe de WORLD SELECT vous remercie d'avoir choisi notre
					plateforme de recrutement à l'international en ligne. Votre
					pré-candidature à cette offre d'emploi a bien été reçue et
					sera automatiquement transférée à nos équipes pour
					traitement.
				</p>

				<div className="pxp-404-fig text-center mt-4 mt-lg-5">
					<img src={Done} alt="Done" />
				</div>

				<div className="mt-4 mt-lg-5 text-center">
					<Link to="/" className="btn rounded-pill pxp-section-cta">
						Vers l'Accueil
						<span className="fa fa-angle-right"></span>
					</Link>
				</div>
			</div>
		</section>
	);
}
