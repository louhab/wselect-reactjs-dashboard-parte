import React from "react";
import RowList from "./CandidateRow/rowList";
export const CandidatesbodyTableList = ({ data }) => {

	return (
		<tbody>
			{data?.map((el, index) => {
			return (  
				<RowList el={el} key={index}/>
			)	
			})}
		</tbody>
	);
};

