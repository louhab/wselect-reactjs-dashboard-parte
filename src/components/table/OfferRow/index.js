import React from "react";
import dayjs from "dayjs";
import { instance } from "../../../lib/axios";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
const MySwal = withReactContent(Swal)
const OfferRowTable = ({ Row, Key }) => {
	const navigate = useNavigate();
	const handleClick = (e) => {
		e.preventDefault();
	  };
	const handlDelete = async(id)=>{
		MySwal.fire({
			title: 'Etes vous sur?',
			text: "cette opération est irréverssible",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Oui, Supprimer!'
		  }).then((result) => {
			if (result.isConfirmed) {
				instance.delete(`/offers/${id}`); 
				MySwal.fire(
				'Supprimé!',
				'Offre est supprimé.',
				'success'
			  )
			  window.location.reload(false)
			}
		  })	   
	}
	const sendToExternelLink = (uuid,employerUuid) => {
		if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
			window.open('http://localhost:3001/candidate-sign-up?uuid='+uuid+'&employerUuid='+employerUuid)	
			} 
		else {
			window.open('http://64.226.123.201:6003/candidate-sign-up?uuid='+uuid+'&employerUuid='+employerUuid)
		}
	};
	return (
		<tr  key={`RowTable${Key}`}>
		<td>
			<a href="#" onClick={handleClick}  >
				<div className="pxp-company-dashboard-job-title">{Row?.title}</div>
				<div className="pxp-company-dashboard-job-location"><span className="fa fa-globe me-1"></span>
					{Row.employer.address}
				</div>
			</a>
		</td>
		<td>
			<div className="pxp-company-dashboard-job-category">
				{Row.employer.companyName}
			</div>
		</td>
		<td>
			<a href="#" onClick={()=>{navigate(`/candidates-offer/${Row.id}`)}} className="pxp-company-dashboard-job-applications">{Row?.PreCandidate?.length} Candidatures</a>
		</td>
		<td>
			<div className="pxp-company-dashboard-job-status">
			{
				Row.status ?
				<span className="badge rounded-pill bg-success">Active</span>:
				<span className="badge rounded-pill bg-danger">Desactive</span>
			}
			</div>
			<div className="pxp-company-dashboard-job-date mt-1">{dayjs(Row?.createdAt).format("YYYY/MM/DD")}</div>
		</td>
		<td>
			<div className="pxp-dashboard-table-options">
				<ul className="list-unstyled">
					<li>
						<button title="copier"><span className="fa fa-copy"></span></button>
					</li>
					<li><button title="Editer" onClick={()=>{navigate(`/offer-edit/${Row.id}`)}}><span className="fa fa-pencil" ></span></button></li>
					<li><button title="Link" onClick={()=>{sendToExternelLink(Row.uuid,Row.employer.uuid)}}><span className="fa fa-external-link"></span></button></li>
					<li><button title="Delete"  onClick={()=>{handlDelete(Row.id )}}><span className="fa fa-trash-o"></span></button></li>
				</ul>
			</div>
		</td>
	</tr>
	);
};

export default OfferRowTable;
