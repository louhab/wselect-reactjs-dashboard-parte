import React from "react";
import dayjs from "dayjs";
const RoW = ({ Row, Key }) => {
	return (
		<tr key={Key}>
					<td><div class="pxp-company-dashboard-subscriptions-plan">{Row.title}</div></td>
					<td><div class="pxp-company-dashboard-subscriptions-plan">{Row.title}</div></td>
					<td><div class="pxp-company-dashboard-subscriptions-status"><span class="badge rounded-pill bg-success">Active</span></div></td>
					<td><div class="pxp-company-dashboard-job-date">{dayjs(Row.createdAt).format("YYYY/MM/DD")} </div></td>
					<td>
						<div class="pxp-dashboard-table-options">
							<ul class="list-unstyled">
								<li><button title="Annuler l'abonnement"><span class="fa fa-ban"></span></button></li>
							</ul>
						</div>
					</td>
		</tr>
	);
};
export default RoW;
