import React from "react";
import UsersRoW from './UsersRow/index'

export const UsersBodyTable = ({ data }) => {
return (
		<tbody>
			{data?.map((el, index) => {
				return ( 
					<UsersRoW key={index} el={el}/>
				)
			})}
		</tbody>
	);
};

