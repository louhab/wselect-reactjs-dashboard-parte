import React from "react";
import Row from './CandidateRow/row'
export const CandidatesbodyTable = ({ data }) => {
	return (
		<tbody>
				{   
					data !=null ? 	data.map((item, index) => {
						return (
							<Row key={index} row={item}/>
						)	
                        	
					})
					:null
				
				}				
		</tbody>
	);
};

