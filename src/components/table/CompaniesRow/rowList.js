import React, {useState,useContext,useEffect} from "react";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { instance } from "../../../lib/axios";
import { AuthContext } from "../../../context/Auth";

import dayjs from "dayjs";
const MySwal = withReactContent(Swal)
const RowList = ({ Row, Key }) => {
    const [user, setUser]= useState({})
    const { auth } = useContext(AuthContext);
	const navigate = useNavigate();
    useEffect(() => {
		instance
            .get(`/users/${auth.id}`)
            .then((res) => {
                setUser(res.data.results);
            })
            .catch((err) => {
				console.log(err)
            });
    }, []);
    const handleDelete = async(id)=>{
		MySwal.fire({
			title: 'Etes vous sur?',
			text: "cette opération est irréverssible",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Oui, Supprimer!'
		  }).then((result) => {
			if (result.isConfirmed) {
				instance.delete(`/employers/${id}`)
				MySwal.fire(
				'Supprimé!',
				'Entreprise est supprimé.',
				'success'
			  )
			  window.location.reload(false)
			}
		  })
	   
	}
	return (
      
        <tr key={Key}>
            
        <td>
            <a href=""  onClick={()=>{navigate(`/company-details/${Row?.id}`)}} >
                <div className="pxp-candidate-dashboard-job-title">{Row.companyName}</div>
                <div className="pxp-candidate-dashboard-job-location"><span className="fa fa-globe me-1"></span>{Row.address }</div>
            </a>
        </td>
        <td>
            
            <div className="pxp-candidate-dashboard-job-category">
            {Row.contactName }
            </div>
        </td>
        <td><div className="pxp-candidate-dashboard-job-category">{Row.email }</div></td>
        {user.role === "Admin" &&
         <td><div className="pxp-candidate-dashboard-job-type">

         <th >{Row?.employerRef?.fullname}</th>
         
         </div></td>
        }
        <td><div className="pxp-candidate-dashboard-job-type">{Row.contactPhone }</div></td>
      
        <td><div className="pxp-candidate-dashboard-job-type">
            <div className="pxp-company-dashboard-job-status"><span className="badge rounded-pill bg-success">{Row.status}</span></div></div>
        </td>
        <td><div className="pxp-candidate-dashboard-job-date mt-1">{dayjs(Row?.createdAt).format("YYYY/MM/DD")}</div></td>
        <td>
            <div className="pxp-dashboard-table-options">
                <ul className="list-unstyled">
                    <li><button title="Voir" onClick={()=>{navigate(`/company-details/${Row?.id}`)}} ><span className="fa fa-eye"></span></button></li>
                    <li><button title="Supprimer" onClick= {()=>{handleDelete(Row?.id)}}><span className="fa fa-trash-o"></span></button></li>
                </ul>
            </div>
        </td>
    </tr>
	);
};

export default RowList;
