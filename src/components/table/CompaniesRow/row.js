import React from "react";
import { useNavigate } from "react-router-dom";
const Row = ({ row, Key }) => {
	const navigate = useNavigate();
	return (
	<tr key={`RowTable${Key}`}>
		<td>
			<div className="pxp-company-dashboard-candidate-name">
				{row?.companyName}
			</div>
		</td>
		 <td><div className="pxp-company-dashboard-candidate-title">{row?.contactName }</div></td>
		<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{row?.address }</div></td>
		<td>
			 <div className="pxp-dashboard-table-options">
				 <ul className="list-unstyled">
					 <li><button title="Consulter" onClick={()=>{navigate(`/company-details/${row?.id}`)}}><span className="fa fa-eye"></span></button></li>
				 </ul>
			</div>
		</td>
	</tr>
	);
};

export default Row;
