import React from "react";
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { instance } from "../../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import dayjs from "dayjs";

const MySwal = withReactContent(Swal);


const Row = ({ el, Key }) => {
    const navigate = useNavigate();
    async function handlStatus(id,newState ) {
		let status = ''
		if(newState === 'Confirmé') {
			status = 'pass'
		}else {
			status = 'fail'
		}

		instance
			.put(`/candidates/${id}`, {
				status:status
			})
			.then((res) => {
				 toast.success(`le condidat est ${newState}`);
				 window.location.reload(false)
			})
			.catch((err) => {
				console.log(err);
			});
	}
	const handleDelete = async(id)=>{
		MySwal.fire({
			title: 'Etes vous sur?',
			text: "cette opération est irréverssible",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Oui, Supprimer!'
		  }).then((result) => {
			if (result.isConfirmed) {
				instance.delete(`/candidates/${id}`)
				MySwal.fire(
				'Supprimé!',
				'Candidature est supprimé.',
				'success'
			  )
			}
			window.location.reload(false)
		  })
	   
	}
    return (

        <tr key={Key}>                    
        <td>
            <Toaster position="bottom-center" reverseOrder={false} />
            <a href="#" onClick={()=>{navigate(`/candidate-details/${el.id}`)}}  >
                <div className="pxp-company-dashboard-job-title">{el?.firstName} {el?.lastName}</div>
                <div className="pxp-company-dashboard-job-location"><span className="fa fa-globe me-1"></span>{el?.employer?.address}</div>
            </a>
        </td>
        <td><div className="pxp-company-dashboard-job-category">{el?.phone}</div></td>
        <td><div className="pxp-company-dashboard-job-category">{el?.offer?.title}</div></td>
            <td>
                <div className="pxp-company-dashboard-job-category">
                    {el?.employer?.companyName}
                </div>
            </td>
        <td>
            <div className="pxp-company-dashboard-job-status">
                {
                    el.status === 'fail'?
                    <span className="badge rounded-pill bg-danger">Refuser</span>:
                    <span className="badge rounded-pill bg-success">Confirmer</span>
                }
            </div>
        </td>
        <td>
            <div className="pxp-company-dashboard-job-date">{dayjs(el?.createdAt).format("YYYY/MM/DD")}</div>
        </td>
        <td>
            <div className="pxp-dashboard-table-options">
                <ul className="list-unstyled">
                    
                    <li><button  title="Consulter" onClick={()=>{navigate(`/candidate-details/${el.id}`)}}> <span className="fa fa-eye"></span></button></li>
                    <li>
                    {
                        el.status === 'pass' ?
                        <button title="Réfuser" onClick={()=>{handlStatus(el.id ,'Réfusé')}}><span className="fa fa-ban"></span></button>
                        : <button title="Confirmer"  onClick={()=>{handlStatus(el.id,'Confirmé' )}}><span className="fa fa-check"></span></button>
                    }
                    </li>
                    <li><button title="Supprimer" onClick={()=>{handleDelete(el.id )}}><span className="fa fa-trash-o"></span></button></li>
                </ul>
            </div>
        </td>
    </tr>
        // <tr key={Key}>
        //     <td>
        //         <Toaster position="bottom-center" reverseOrder={false} />
        //         <a href="#" onClick={() => { navigate(`/candidate-details/${candidate.id}`) }}>
        //             <div className="pxp-company-dashboard-job-title">{`${candidate.firstName} ${candidate.lastName}`}</div>
        //         </a>
        //     </td>
        //     <td><div className="pxp-company-dashboard-job-category">{candidate.phone}</div></td>
        //     <td>
        //         <div className="pxp-company-dashboard-job-status" >
        //             <span className={`badge rounded-pill ${candidate.status === 'fail' ? 'bg-danger' : 'bg-success'}`}>
        //                 {statusMap[candidate.status].label}
        //             </span>
        //         </div>
        //     </td>
        //     <td>
        //         <div className="pxp-company-dashboard-job-date">{dayjs(candidate.createdAt).format("YYYY/MM/DD")}</div>
        //     </td>
        //     <td>
        //         <div className="pxp-dashboard-table-options">
        //             <ul className="list-unstyled">
        //                 <li><button title="Consulter" onClick={() => { navigate(`/candidate-details/${candidate.id}`) }}> <span className="fa fa-eye"></span></button></li>
        //                 <li>
        //                     <button
        //                         title={statusMap[candidate.status].oppositeLabel}
        //                         onClick={() => { handleStatusChange(candidate.id, candidate.status) }}
        //                     >
        //                         <span className={`fa ${candidate.status === 'pass' ? 'fa-ban' : 'fa-check'}`}></span>
        //                     </button>
        //                 </li>
        //                 <li><button title="Supprimer" onClick={() => { handleDelete(candidate.id) }}><span className="fa fa-trash-o"></span></button></li>
        //             </ul>
        //         </div>
        //     </td>
        // </tr>
    );
};

export default Row;
