import React  from "react";
import OfferRowTable from './OfferRow/index'


export const OffersBodyTable = ({ data }) => {

	return (
		<tbody>
			{data?.map((el, index) => {
				return (
					<OfferRowTable Row={el} key={index}/>
				)
			})}
		</tbody>
	);
};

