import React  from "react";
import CompaniesRow from "./CompaniesRow/row"
export const CompaniesBodyTable = ({ data }) => {
	return (
		<tbody>
			{data?.map((el, index) => {
				return (
					<CompaniesRow row={el} key={index}/>
				)
			})}
		</tbody>
	);
};

