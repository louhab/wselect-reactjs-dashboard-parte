import React from "react";
import Row from './AbonnementRow/index'
export const AbonnementsBodyTable = ({ data }) => {
	return (
		<tbody>
			{data?.map((el, index) => {
				return (
					<Row key={index} Row= {el} />
				);
			})}
		</tbody>
	);
};

