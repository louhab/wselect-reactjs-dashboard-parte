import React from "react";
import Row from "./CandidateOnOfferRow/index"
export const CandidatesOnOfferTable = ({ data }) => {
return (
		<tbody>
			{data?.map((el, index) => {
			return (  
				<Row key={index} el={el} />
			)
			})}
		</tbody>
	);
};

