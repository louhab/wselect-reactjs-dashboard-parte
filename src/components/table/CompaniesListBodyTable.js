import React  from "react";
import RowList from "./CompaniesRow/rowList";
export const CompaniesListBodyTable = ({ data }) => {
return (
		<tbody>
			{data?.map((el, index) => {
				return (
					<RowList key={index} Row={el} />
				)
			})}
		</tbody>
	);
};

