import React from "react";
import dayjs from "dayjs";
import { useNavigate } from "react-router-dom";
import { instance } from "../../../lib/axios";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
const MySwal = withReactContent(Swal)
const UsersRoW = ({ el, Key }) => {
	const navigate = useNavigate();
	const handleDelete = async(id)=>{
		MySwal.fire({
			title: 'Etes vous sur?',
			text: "cette opération est irréverssible",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Oui, Supprimer!'
		  }).then((result) => {
			if (result.isConfirmed) {
				instance.delete(`/users/${id}`); 
				MySwal.fire(
				'Supprimé!',
				'Utilisateur est supprimé.',
				'success'
			  )
			  window.location.reload(false)

			}
		  })
	   
	}
	
	return (
		<tr key={Key}>
					<td><div className="pxp-company-dashboard-job-title">{el.fullname }</div></td>
					<td>
						<div className="pxp-company-dashboard-job-category">{el.email }</div>
					</td>
					<td><div className="pxp-company-dashboard-job-category">{el.phone} </div></td>
					<td> <div className="pxp-company-dashboard-job-status">
						{
							el.role==='Admin'?
							<span className="badge rounded-pill bg-success">{el.role }</span>
							:<span className="badge rounded-pill bg-info">{el.role }</span>
							
						}
						</div></td>
					<td>
						<div className="pxp-company-dashboard-job-date mt-1">{dayjs(el?.createdAt).format("YYYY/MM/DD")}</div>
					</td>
					<td>
						<div className="pxp-dashboard-table-options">
							<ul className="list-unstyled">
								<li><button title="Editer" onClick={()=>{navigate(`/edit-users/${el.id}`)}}><span className="fa fa-pencil" ></span></button></li>
								{
									el.role!= 'Admin'?
									<li><button title="Delete" onClick={()=>handleDelete(el.id)}><span className="fa fa-trash-o" ></span></button></li>
									:null
								}

							</ul>
						</div>
					</td>
				</tr>
	);
};

export default UsersRoW;
