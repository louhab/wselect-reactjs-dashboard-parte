import React from "react";
import { useNavigate } from "react-router-dom";
const Row = ({ row, Key }) => {
	const navigate = useNavigate();
	return (
		<tr key={Key}>				
		<td ><div className="pxp-company-dashboard-candidate-name">{row.firstName} {row.lastName }</div></td>
		<td ><div className="pxp-company-dashboard-candidate-title">{row.phone }</div></td>
		<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{row?.offer[0]?.offer?.title}</div></td>
		<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{row?.offer[0]?.offer?.employer?.companyName}</div></td>
		<td>
			<div className="pxp-dashboard-table-options">
				<ul className="list-unstyled">
					<li><button  onClick={()=>{navigate(`/candidate-details/${row.id}`)}}><span className="fa fa-eye"></span></button></li>
				</ul>
			</div>
		</td> 
		</tr>
	);
};

export default Row;
