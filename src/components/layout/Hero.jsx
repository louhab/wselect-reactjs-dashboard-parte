import React from "react";
// import LogoOne from "../assets/images/hero-logo-1.svg";
// import LogoTwo from "../assets/images/hero-logo-2.svg";
// import LogoThree from "../assets/images/hero-logo-3.svg";
// import LogoFour from "../assets/images/hero-logo-4.svg";
// import LogoFive from "../assets/images/hero-logo-5.svg";
// import LogoSix from "../assets/images/hero-logo-6.svg";

function Hero() {
    return (
        <>
            <section className="pxp-hero vh-100" style={{ backgroundColor: 'var(--pxpSecondaryColor)' }}>
                <div className="pxp-hero-caption">
                    <div className="pxp-container">
                        <div className="row pxp-pl-80 align-items-center justify-content-between">
                            <div className="col-12 col-xl-6 col-xxl-5">

                                <h1 >Trouvez <span style={{ color: "var(--pxpMainColor)" }}>l'emploi</span> qui vous convient</h1>
                                <div className="pxp-hero-subtitle mt-3 mt-lg-4">Search your career opportunity through <strong>12,800</strong> jobs</div>

                                {/* <div className="pxp-hero-form pxp-hero-form-round mt-3 mt-lg-4">
                                    <form className="row gx-3 align-items-center" action="jobs-list-1.html">
                                        <div className="col-12 col-sm">
                                            <div className="mb-3 mb-sm-0">
                                                <input type="text" className="form-control" placeholder="Job Title or Keyword" />
                                            </div>
                                        </div>
                                        <div className="col-12 col-sm pxp-has-left-border">
                                            <div className="mb-3 mb-sm-0">
                                                <input type="text" className="form-control" placeholder="Location" />
                                            </div>
                                        </div>
                                        <div className="col-12 col-sm-auto">
                                            <button><span className="fa fa-search"></span></button>
                                        </div>
                                    </form>
                                </div> */}

                                <div className="pxp-hero-searches-container">
                                    <div className="pxp-hero-searches-label">Popular Searches</div>
                                    <div className="pxp-hero-searches">
                                        <div className="pxp-hero-searches-items">
                                            <a href="jobs-list-1.html">Work from home</a>
                                            <a href="jobs-list-1.html">Part-time</a>
                                            <a href="jobs-list-1.html">Administration</a>
                                            <a href="jobs-list-1.html">Finance</a>
                                            <a href="jobs-list-1.html">Retail</a>
                                            <a href="jobs-list-1.html">IT</a>
                                            <a href="jobs-list-1.html">Engineering</a>
                                            <a href="jobs-list-1.html">Sales</a>
                                            <a href="jobs-list-1.html">Manufacturing</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="pxp-hero-right-bg-card pxp-with-image pxp-has-animation pxp-animate" style={{ backgroundImage: `var(--backgroundImage)` }}></div>
            </section>

        </>
    )
}

export default Hero;
