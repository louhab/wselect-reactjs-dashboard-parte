
import { instance } from "../../../lib/axios";
import React, { useState, useEffect } from "react";
import { CandidatesbodyTable } from './../../table/CandidatesBodyTable';
export default function  Candidates() {
    const [condidates, setCondidates] = useState([]);
    const fetchCandidatures = async() =>{
        const {data} = await instance.get(`/candidates?skip=`+0+`&take=`+4); 
        setCondidates(data.results.candidates)
    }
    useEffect(()=>{
        fetchCandidatures()   
    }, [])
    return (
        <>
          <div className="mt-4 mt-lg-5">
				{
					condidates?.candidates?.length  === 0 ?
					<p style={{textAlign:'center'}}>
				
						Aucune candidature pour le moment.
					</p>
					:
					<div>
						 <h2>Candidatures récente</h2>
						<div className="table-responsive">
							<table className="table table-hover align-middle">
                            <CandidatesbodyTable data={condidates.candidates}/>
							</table>
						</div> 
					</div>
				}
			</div>
        </>
    )
}