import React, { useEffect, useState , useContext } from "react";
import { Link, NavLink } from "react-router-dom";
import onContentScroll from "../../../lib/main/onContentScroll";
import handlePreloader from "../../../lib/main/handlePreloader";
import windowResizeHandler from "../../../lib/main/windowResizeHandler";
import $ from "jquery";
import Logo from "../../../assets/images/logo-transparant.png";
import useCurrentPath from "../../../hooks/useCurrentPath";
import { AuthContext } from "../../../context/Auth";
import { instance } from "../../../lib/axios";
const SidePanel = ({ show, setShow, setPosition, position, panel }) => {
	const path = useCurrentPath();
	const [user, setUser] = useState({});
	const [users, setUsers] = useState([]);
	const { auth } = useContext(AuthContext);
	useEffect(() => {
		instance
			.get(`/users?skip=`+0+`&take=`+3)
			.then((res) => {
				const data = res?.data?.results.allusers;
				setUsers(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);
	useEffect(() => {
		
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				const data = res?.data?.results;
				setUser(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);

	useEffect(() => {

		window.onscroll = function () {
			onContentScroll();
		};
		handlePreloader();

		windowResizeHandler();

		$(window).resize(function () {
			windowResizeHandler();
		});

		window.scrollTo(0, 0);
	}, []);

	return (
		<>
			{show ? (
				<div
					className="offcanvas-backdrop fade show d-lg-none"
					style={{ zIndex: 50 }}
				></div>
			) : null}

			<div
				className={`pxp-dashboard-side-panel ${show && "d-block"}`}
				style={{
					transition: "all 0.3s ease-out-in",
					backgroundColor: "#E4F0FA",
				}}
			>
				<div className="position-fixed">
					<div
						className="d-flex justify-content-between align-items-center"
						style={{ width: "250px" }}
					>
						
						<div className="pxp-logo">

							<NavLink to="/" className="pxp-animate ">
								<img
									style={{ height: "37px" }}
									src={Logo}
									alt="logo"
								/>
							</NavLink>
						</div>
						<button
							type="button"
							className="btn-close text-reset d-lg-none"
							data-bs-dismiss="oncanvas"
							aria-label="Close"
							onClick={() => {
								setShow(0);
							}}
						></button>
					</div>
							
					<nav className="mt-3 mt-lg-4 d-flex justify-content-between flex-column position-fixed">
					<div className="pxp-dashboard-side-label">Administration</div>
						<ul className="list-unstyled">
							
							{panel.map((elem, index) => {
								const temp =
									path === elem.path ? "pxp-active" : "";
								return (<>

									<li
										key={index}
										className={temp}
									>
										<NavLink
											to={elem.path}
										>
											<span
												className={elem.spanClass}
											></span>
											{elem.title}
										</NavLink>
									</li>
									</>

								);
							})}
						</ul>
						{

							user.role === "Admin" &&
							<div className="pxp-dashboard-side-label mt-3 mt-lg-4">
									For Admin
							</div> 
						}
						{user.role === "Admin" &&
							<ul className="list-unstyled">
								<li >
									<Link onClick={() => {setPosition(20);}}
									to={"/users"}
									className="d-flex justify-content-between align-items-center"
									>
									<div><span className="fa fa-envelope-o"></span>Utilisateurs  </div>
									<span className="badge rounded-pill"> {users.length }</span>
									</Link>
								</li>
								<li>
								<Link onClick={() => {setPosition(20);}}
									to={"/abonnements"}
									className="d-flex justify-content-between align-items-center"
									>
										<span className="fa fa-credit-card"></span>Abonnements
									</Link>
								</li>
						</ul>
						}
					</nav>
				</div>
			</div>
		</>
	);
};

export default SidePanel;
