
import { instance } from "../../../lib/axios";
import React, { useState, useEffect } from "react";
export default function  Header() {
    const [offerCount, setCountOffers] = useState([]);
    const [condidates, setCondidates] = useState([]);
    const [entreprises, setEntreprises] = useState([]);
    const [aboonement, setAboonement] = useState([]);
    const fetchOffers = async() =>{
        const {data} = await instance.get(`/offers?skip=`+0+`&take=`+100);
        setCountOffers(data.results.offerCount) 
    }
    const fetchCandidatures = async() =>{
        const {data} = await instance.get(`/candidates?skip=`+0+`&take=`+100); 
        setCondidates(data.results.candidatesCount) 
    }
    const fetchEntreprises = async() =>{
        const {data} = await instance.get(`/employers?skip=`+0+`&take=`+100);
        setEntreprises(data.results.employersCount)

    }
    const fetchAboonement = async() =>{
        const {data} = await instance.get(`/abonnements?skip=`+0+`&take=`+100);
        setAboonement(data.results.allAbonnements)

    }
    useEffect(()=>{
        fetchOffers() 
        fetchCandidatures()   
        fetchEntreprises()
        fetchAboonement()
    }, [])
    return (
        <>
				<div className="row mt-4 mt-lg-5 align-items-center">
                    <div className="col-sm-6 col-xxl-3">
                        <div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
                            <div className="pxp-dashboard-stats-card-icon text-primary">
                                <span className="fa fa-file-text-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{offerCount}</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Offres </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-xxl-3">
                        <div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
                            <div className="pxp-dashboard-stats-card-icon text-success">
                                <span className="fa fa-user-circle-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{condidates}</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Candidatures</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-xxl-3">
                        <div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
                            <div className="pxp-dashboard-stats-card-icon text-warning">
                                <span className="fa fa-envelope-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{entreprises}</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Entreprises</div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-xxl-3">
                        <div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
                            <div className="pxp-dashboard-stats-card-icon text-danger">
                                <span className="fa fa-bell-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{aboonement?.length}</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Abonnements</div>
                            </div>
                        </div>
                    </div>
                </div>
        </>
    )
}