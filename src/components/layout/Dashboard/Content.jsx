import React from "react";

function Content() {
	return (
		<>
			<div className="pxp-dashboard-content-details">
				<h1>Dashboard </h1>
				<div
					style={{
						minHeight: "80vh",
					}}
				></div>
			</div>

			<footer>
				<div className="pxp-footer-copyright pxp-text-light">
					© {new Date().getFullYear()} World select. All Right
					Reserved.
				</div>
			</footer>
		</>
	);
}

export default Content;
