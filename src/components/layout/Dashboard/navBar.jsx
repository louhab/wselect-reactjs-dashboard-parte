import React, { useContext } from "react";
import { Link } from "react-router-dom";
import ComLogo from "../../../assets/images/company-logo-1.png";
import { AuthContext } from "../../../context/Auth";
import toast, {Toaster} from "react-hot-toast";
import { instance } from "../../../lib/axios";


const NavBar = ({ setShow, candidate }) => {
	
    const [user, setUser] = React.useState({});
    const { auth } = useContext(AuthContext);
    const unsecuredCopyToClipboard = (text) => { const textArea = document.createElement("textarea"); textArea.value=text; document.body.appendChild(textArea); textArea.focus();textArea.select(); try{document.execCommand('copy')}catch(err){console.error('Unable to copy to clipboard',err)}document.body.removeChild(textArea)};

    const copyToClipboard = (content) => {
        if (window.isSecureContext && navigator.clipboard) {
        toast.success("Lien copié");

          navigator.clipboard.writeText(content);
        } else {
          unsecuredCopyToClipboard(content);
        }
      };
    React.useEffect(() => {
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				const data = res?.data?.results;
				setUser(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);
	const { logout } = useContext(AuthContext);
	return (
		<div
			style={{
				top: 5,
				padding: 30,
				paddingRight: 60,
				right: 0,
				zIndex: 0,
			}}
			className="pxp-dashboard-content-header "
		>
			<div className="pxp-nav-trigger navbar pxp-is-dashboard d-lg-none">
				<Link
					onClick={() => {
						setShow(1);
					}}
				>
					<div className="pxp-line-1"></div>
					<div className="pxp-line-2"></div>
					<div className="pxp-line-3"></div>
				</Link>
			</div>
			<Toaster position="bottom-center" reverseOrder={false} />

			<nav className="pxp-user-nav pxp-on-light">
            <button 
			  onClick={() =>
                copyToClipboard(`${process.env.REACT_APP_API_WEBSITE}/employer-sign-up?ref=${user?.id}`)}
                                className="btn rounded-pill pxp-card-btn"
				>
						
							Copier le lien
			</button>

				<div className="dropdown pxp-user-nav-dropdown">
					<div						
						className="dropdown-toggle"
						data-bs-toggle="dropdown"
						style={{
							cursor: "pointer",
						}}
					>
						<div
							className="pxp-user-nav-avatar pxp-cover"
							style={{
								backgroundImage: `url(${ComLogo})`,
							}}
						>
							<Link
								role="button"
								className="text-dark"
								data-bs-toggle="offcanvas"
								data-bs-target="#pxpMobileNav"
								aria-controls="pxpMobileNav"
								to={"#"}
							>
								<div className="pxp-line-1"></div>
								<div className="pxp-line-2"></div>
								<div className="pxp-line-3"></div>
							</Link>
						</div>
						<div className="pxp-user-nav-name d-none d-md-block ">
							{candidate}
						</div>
					</div>
					<ul className="dropdown-menu dropdown-menu-end">
                        <li>
                            <Link
                            className="dropdown-item"
                            to= {'/edit-profile/'+auth.id}
                            >
                             Editer votre profile
                            </Link>
                        </li>
						<li>
							<Link
								className="dropdown-item"
								to="/"
								onClick={() => {
									localStorage.clear("ws_admin_token");
									logout();
								}}
							>
								Se déconnecter
							</Link>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	);
};

export default NavBar;
