import React from 'react';

function CandidateComponent() {
  // Assuming 'candidate' is a boolean variable determining whether to show the profile button

  return (
    <>

        <td>
          <div className="pxp-dashboard-table-options">
            <ul className="list-unstyled">
              <li>
                <button title="View profile">
                  <span className="fa fa-eye"></span>
                </button>
              </li>
            </ul>
          </div>
        </td>
  
    </>
  );
}

export default CandidateComponent;
