
import { instance } from "../../../lib/axios";
import React, { useState, useEffect } from "react";
import { CompaniesBodyTable } from "../../table/CompaniesBodyTable";
export default function  Companies() {
    const [companies, setCompanies] = useState([]);
    const fetchCompanies = async() =>{
        const {data} = await instance.get(`/employers?skip=`+0+`&take=`+4); 
        setCompanies(data.results.employers)
	    }
    useEffect(()=>{
        fetchCompanies()   
    }, [])
		return (
			<>
			  <div className="mt-4 mt-lg-5">
				{
					companies.length === 0 ?
					<p style={{textAlign:'center'}}>
						Aucune entreprise pour le moment.
					</p>
					:
					<div>
						<h2>Entreprises récente</h2>
						<div className="table-responsive">
							<table className="table table-hover align-middle">
								<CompaniesBodyTable data={companies} />
							</table>
						</div> 
					</div>
				}
				
			</div>
			</>
		)   
}