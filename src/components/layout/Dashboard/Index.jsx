import React, { useContext,useState } from "react";
import { AuthContext } from "../../../context/Auth";
import { instance } from "../../../lib/axios";
import Header from './Header'
import Candidates from './Candidates'
import Companies from './Companies'

export default function Index() {
	const { auth } = useContext(AuthContext);
	const [user, setUser] = React.useState({});
	const [loading, setLoading] = useState(true);
	React.useEffect(() => {
		setTimeout(() => {
			setLoading(false);
		}, 1000); 
	  }, []);
	React.useEffect(() => {
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				setUser(res?.data?.results);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);

	if (!user?.id)
		return (
			<>
				<footer>
					<div className="pxp-footer-copyright pxp-text-light">
						© {new Date().getFullYear()} World select. All Right
						Reserved.
					</div>
				</footer>
			</>
		);

	return (
		<>
			{
				loading ? (	
				<div className="pxp-dashboard-content-details">
				<div className="text-center">
					<div className="spinner-border" role="status">
						<span className="sr-only">Loading...</span>
					</div>
				</div>
		</div>	):(
					<div>
					<div className="pxp-dashboard-content-details"> 
						<h1>Dashboard</h1>
						<p className="pxp-text-light">Bienvenue sur Wselect!</p>
							<Header/>
							<Companies/>
							 <Candidates/>
					</div>
			<footer>
				<div className="pxp-footer-copyright pxp-text-light">
					© {new Date().getFullYear()} World select. All Right
					Reserved.
				</div>
			</footer>
					</div>
				)
			}
		</>
	);
}
