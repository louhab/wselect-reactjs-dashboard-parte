import React, { useState, createContext, useEffect, useContext } from "react";
import { useLocation, Navigate, Outlet } from "react-router-dom";
import jwt_decode from "jwt-decode";
import { instance } from "../lib/axios";
export const AuthContext = createContext();

function AuthProvider(props) {
	const initial = { type: null, id: null };
	const [auth, setAuth] = useState(initial);
	const [loading, setloading] = useState(true);
	const logout = () => {
		setAuth({});
		localStorage.clear();
	};
	useEffect(() => {
		const token = localStorage.getItem("ws_admin_token");
		if (token) {
			(async () => {
				const {
					data: { success, results },
				} = await instance.get("/users/token");

				if (success) {
					const decoded = jwt_decode(token);
					setAuth({
						token,
						user: results,
						id: decoded?.id,
					});

					setloading(false);
				} else {
					logout();
					setloading(false);
				}
			})();
		} else {
			logout();
			setloading(false);
		}
	}, []);
	return (
		<AuthContext.Provider
			value={{ auth, setAuth, logout, loading, setloading }}
		>
			{props.children}
		</AuthContext.Provider>
	);
}

/**
 * in general cases I dont need to check the local storage
 */
export const RequireAuth = () => {
	const { auth, loading } = useContext(AuthContext);

	const location = useLocation();

	if (loading) return <div></div>;

	return auth?.token ? (
		<Outlet />
	) : (
		<Navigate to="/auth/login" state={{ from: location }} replace />
	);
};
/**
 * I need  to check if the user is authenticated and he has the right role to get in :
 */
export const ProtectedRoute = () => {
	const { auth, loading } = useContext(AuthContext);
	const location = useLocation();
	if (loading) return <div></div>;
	return auth?.token  && auth.user.user.role === 'Admin'? (
		<Outlet />
	  ) : (
		<Navigate to="/auth/login" state={{ from: location }} replace />
	  );	
};
export default AuthProvider;
