import React , {useEffect,useState}from 'react';
import { instance } from "../../lib/axios";
import Creatable from 'react-select/creatable';
import { useLocation } from 'react-router-dom';
import toast, { Toaster } from "react-hot-toast";
import bcrypt from 'bcryptjs';
export default function EditCpmpany() {
    const location = useLocation();
    const [loading, setLoding] = useState(false);
    const [employer, setEmployer]=useState('')
    const [name, setName]=useState('')
    const [phone, setPhone]=useState('')
    const [email, setEmail]=useState('')
    const [webSite, setWebSite]=useState('')
    const [interlocuteur, setInterlocuteur]=useState('')
    const [adress,setAdress]= useState('')
    const [needs, setNeeds]=useState('')
    const [password, setPassword]= useState('');
    let [options, setOptions] = useState([]) 
    let id = location.pathname.replace(/^\D+/g, '')
    useEffect(()=>{
        setLoding(true)
        instance.get(`employers/${id}`)
        .then((res) => {
            setEmployer(res.data.results)
            setLoding(false)
            console.log('employer', employer)
          })
    }, [])
	useEffect(() =>{
        let array = []
        setLoding(true)
		instance.get('/employers?skip=0&take=10')
		.then(res => {
            res.data.results.allemployers.forEach(employer=>{
                array.push({
                    value: employer.id,
                    label: employer.companyName,
                }) 
            })  
        }) 
         setOptions(array)

	},[])
    async function onSubmit(e) {
        e.preventDefault()
        let data = {
            companyName: name === '' ? employer.companyName : name,
            contactName:interlocuteur === '' ? employer.contactName : interlocuteur,
            contactPhone:phone === '' ? employer.contactPhone : phone,
            address:adress === '' ? employer.address : adress,
            email:email === '' ? employer.email : email,
            remarks:needs === '' ? employer.remarks : needs,
            website:webSite === ''? employer.webSite : webSite,
            password:password === '' ? bcrypt.hashSync(employer.password ,'$2a$10$CwTycUXWue0Thq9StjUM0u')  :  bcrypt.hashSync(password , '$2a$10$CwTycUXWue0Thq9StjUM0u')
        }
        instance.put(`/employers/${id}`,data)
        .then((response)=>{
            toast.success("Bravo!,l'entreprise est modifié");
            setTimeout(() => {
				window.location.reload(false);
			}, 500);
        })
        .catch((erreur)=>{
            toast.error("Une erreur a été produite", {
				style: {
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
					fontSize: "14px",
				},
			});
            console.log(erreur);
        })
    }
    const borderStyles = {
        control: (styles) => ({ ...styles, backgroundColor: 'white' , borderRadius:'20px',height: '54px' }),
      };
      const initialValue =  { value : employer.companyName ,label: employer.companyName}
      const handlePhoneChange = (event) => {
        setPhone(event.target.value)
        }
      const handleEmailChange = (event) => {
        setEmail(event.target.value)
      }  
      const handleWebSiteChange = (event) => {
        setWebSite(event.target.value)
      }
      const handleContactNameChange = (event) => {
        setInterlocuteur(event.target.value)
      }
      const handleAdresseChange = (event) => {
        setAdress(event.target.value)
      }
      const handleRemarksChange = (event) => {
        setNeeds(event.target.value)
      }
      const handlePasswordChange = (event) => {
        setPassword(event.target.value)
      }
return (
    
        <div className="pxp-dashboard-content-details">
            {
             loading ? 
				<div className="text-center">
					<div className="spinner-border" role="status">
						<span className="sr-only">Loading...</span>
					</div>
				</div>
             : 
            <div>
            <Toaster position="bottom-center" reverseOrder={false} />
            <h2 className="pxp-section-h2 text-center">
                Modifié l'entreprise <span className="badge rounded-pill bg-primary" >{employer.companyName}</span>
            </h2>
            <form  className="mt-4" onSubmit={onSubmit}>
                <label htmlFor="pxp-company-name" className="form-label">Sélectionner ou ecrire le nom du l'entreprise : </label>
                    <Creatable options={options}  onChange={(option)=>{setName(option.label)}}  styles={borderStyles} defaultValue={initialValue}/>
                <label htmlFor="pxp-company-name" className="form-label">Numéro de téléphone</label>
                <input defaultValue={employer.contactPhone} onChange={handlePhoneChange} type="tel"className="form-control"/>                
                <label htmlFor="pxp-company-name" className="form-label">Courriel</label>
                <input  type="email" id="pxp-company-name" className="form-control" defaultValue={employer.email}  onChange={handleEmailChange}/>
                <label htmlFor="pxp-company-name" className="form-label">Password</label>
                <input  type="password" id="pxp-company-name" className="form-control" defaultValue={employer.password}  onChange={handlePasswordChange}/>
                <label htmlFor="pxp-company-name" className="form-label">Site web</label>
                <input  type="text" id="pxp-company-name" className="form-control"  defaultValue={employer.website}  onChange={handleWebSiteChange}/>
                <label htmlFor="pxp-company-name" className="form-label">Interlocuteur</label>
                <input  type="text" id="pxp-company-name" className="form-control"  defaultValue={employer.contactName}  onChange={handleContactNameChange} />
                <label htmlFor="pxp-company-name" className="form-label">Adresse</label>
                <input  type="text" id="pxp-company-name" className="form-control" defaultValue={employer.address}  onChange={handleAdresseChange}/>
                <label htmlFor="pxp-company-name" className="form-label">Besoins spécifiques</label>
                <textarea  type="text" id="pxp-company-name" className="form-control" onChange={handleRemarksChange}  defaultValue={employer.remarks} />
                <button className="btn rounded-pill pxp-section-cta d-block mt-4"
                     type="submit" style={{ width: "100%" }}>
                     Modifier 
                </button>
			</form>
                </div>
            }
      
        </div>
    );
}