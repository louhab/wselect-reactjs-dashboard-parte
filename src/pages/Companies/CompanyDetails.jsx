import React, { useState, useEffect,useContext } from "react";
import { instance } from "../../lib/axios";
import { useLocation } from 'react-router-dom';
import { AuthContext } from "../../context/Auth";
import { useNavigate } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import Comment from "./Comment";
import Log from './Log';
const Contract = {
	id : 1,
	name : 'Contrat Wselect',
	path : '8_contrat_wselect.pdf'
}

export default function CompanyDetails() {
	const [logs,setLogs]= useState([])
	const location = useLocation();
	const navigate = useNavigate();
	let id = location.pathname.replace(/^\D+/g, '')
	const [file, setFile] = useState('');
	const [status, setStatus] =useState('');
	const [company, setCompany] = useState([]);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const [user, setUser] = useState({});
	const { auth } = useContext(AuthContext);
	const [docs,setDocs] = useState([]);
	const [comments,setComments] = useState([]);
	const [userComments, setUserComments ] = useState([]);
	const [countOfCandidate , setCountOfCandidate] =useState(0)
	const [employersContracts,setEmployersContracts]= useState([]);
	const [noContractFound, setnoContractFound] = useState(false);
	const downloadEmployerContract = () => {
		var replacedString = employersContracts[0].path.replace('public', 'static');
		window.open(`${process.env.REACT_APP_API_STATIC}/${replacedString}`, '_blank');
	}
	useEffect(()=>{
		instance.get('/logs')
		.then((res)=>{
            setLogs(res.data.results)
			})

    }, [])
	useEffect(()=>{
		instance.get('/documents')
		.then(res=>{
            setDocs(res.data.results);
        })
	},[])
	useEffect(() => {
		instance
            .get(`/employers/${id}`)
            .then((res) => {
				setEmployersContracts(res.data.results.EmployerContrat)
                setCompany(res.data.results);
				setComments(res.data.results.comment.filter((comment) => comment?.userId != auth.id ) );
                setUserComments(res.data.results.comment.filter((comment) => comment?.userId === auth.id )  )
                setLoading(false);
				let CountOfCandidate = 0 ;
				res.data.results.Offer.forEach(element => {
					CountOfCandidate += element.canidate.length
				});
				setCountOfCandidate(CountOfCandidate)
				res.data.results.EmployerContrat.length === 0 ?  setnoContractFound(true) : setnoContractFound(false)
				console.log(res.data.results.EmployerContrat)
            })
            .catch((err) => {
                setError(true);
                setLoading(false);
				console.log(err)
            });

    }, [id]);
	useEffect(() => {
		instance
            .get(`/users/${auth.id}`)
            .then((res) => {
                setUser(res.data.results);
            })
            .catch((err) => {
				console.log(err)
            });
    }, []);
	useEffect(() => {
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				const data = res?.data?.results;
				setUser(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);
	 const handleStatusUpdate = ()=>{
		const form = {
			status: status === '' ? 'Pas_Interesse' : status
		}
		instance.put(`/employers/${id}`,form)
		.then((res) => {
			toast.success("Bravo!,status est modifié");
			setTimeout(() => {
				window.location.reload(false);
			}, 500);
		})
		instance.post('/logs',{
			action : `Le status de l'entreprise ${company.companyName} a été mise a jour à ${status===''?'Client':status} par ${user.fullname}`
		})
	 }

	return (
		<>
		   {
			loading? (
						<div className="pxp-dashboard-content-details">
						<div className="text-center">
							<div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
						</div>
					   </div>

						) : error? (
							<div>Error...</div>
						) : (	
						<div>
								<Toaster position="bottom-center" reverseOrder={false} />
						   		<div className="pxp-dashboard-content-details ">
								<div className="mt-4 mt-lg-5">
									<div className="row justify-content-between align-content-center">
										<div className="col-auto order-2 order-sm-1">
											<div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
												<h1> {company?.companyName} </h1>
												{
													company.status ===	'Pas_Interesse' ?
													<span className="badge rounded-pill bg-success" style={{marginLeft:'10px'}}>Pas Interessé</span>
													:
													<span className="badge rounded-pill bg-success" style={{marginLeft:'10px'}}>{company.status}</span>

												}
											</div>
										</div>
										<div className="col-auto col-4 order-1 order-sm-2">
											<div className="pxp-company-dashboard-jobs-bulk-actions mb-3">
												<select className="form-select" onChange={(e)=>{setStatus(e.target.value)}} >
													<option  disabled selected>Status...</option>
													{
														user.role === 'Admin' ? <option  value="Nd">Nd</option>
														: ''
													}
													<option value="Pas_Interesse">Pas intéressé</option>
													<option  value="Injoignable">Injoignable</option>
													{
														user.role === 'Admin' ? <option  value="Contract">Contract</option>
														: ''
													}
													<option  value="Devis">devis</option>
													<option  value="Client">client</option>
												</select>
												<button className="btn btn-secondary" style={{width:'127px', marginLeft:'13px'}}  onClick={()=>{handleStatusUpdate()}}><span className="fa fa-pencil"></span> Editer</button>
											</div>
											
										</div>
									</div>
								</div>
								<div className="row">
									<div className="col-9">
										<div className="mt-3 mt-lg-4 pxp-single-job-side-panel">
											<div className="pxp-single-job-side-company">

												<div className="pxp-single-job-side-company-profile">
													<div className="pxp-single-job-side-company-name"> {company?.companyName} </div>
													<a style={{cursor:'pointer'}} onClick={()=>{navigate(`/edit-company/${id}`)}}><span className="fa fa-pencil"></span> Modifier l'entreprise </a>
												</div>
											</div>
											<div className="mt-4">
												<div className="pxp-single-job-side-info-label pxp-text-light">Nom du contact</div>
												<div className="pxp-single-job-side-info-data">{company?.contactName}</div>
											</div>
											<div className="mt-4">
												<div className="pxp-single-job-side-info-label pxp-text-light">Email</div>
												<div className="pxp-single-job-side-info-data">{company?.email}</div>
											</div>
											<div className="mt-4">
												<div className="pxp-single-job-side-info-label pxp-text-light">Téléphone</div>
												<div className="pxp-single-job-side-info-data">{company?.contactPhone}</div>
											</div>
											<div className="mt-4">
												<div className="pxp-single-job-side-info-label pxp-text-light">Adress</div>
												<div className="pxp-single-job-side-info-data">{company?.address}</div>
											</div>
										</div>
									</div>
									<div className="col-3">
										<div className="mt-3 mt-lg-4">
											<div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
												<div className="pxp-dashboard-stats-card-icon text-primary">
													<span className="fa fa-file-text-o"></span>
												</div>
												<div className="pxp-dashboard-stats-card-info">
													<div className="pxp-dashboard-stats-card-info-number"> {company?.Offer?.length} </div>
													<div className="pxp-dashboard-stats-card-info-text pxp-text-light">Offres</div>
												</div>
											</div>
										</div>
										<div className="mt-3 mt-lg-4">
											<div className="pxp-dashboard-stats-card bg-success bg-opacity-10 mb-3 mb-xxl-0">
												<div className="pxp-dashboard-stats-card-icon text-success">
													<span className="fa fa-user-circle-o"></span>
												</div>
												<div className="pxp-dashboard-stats-card-info">
													<div className="pxp-dashboard-stats-card-info-number">{countOfCandidate}</div>
													<div className="pxp-dashboard-stats-card-info-text pxp-text-light">Candidatures</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								</div>
								<div className="pxp-dashboard-content-details mb-4 mb-lg-4 mt-4">
									<h1>Dossier Entreprise</h1>
									<div className="mt-4 mt-lg-5">
										<div className="row  justify-content-between align-content-center">
											<div className="col-6">
												<div className=" pxp-single-job-side-panel">
												<div className="table-responsive">
													<table className="table table-hover align-middle">
														<thead>
															<tr>
																<th>Documents</th>
																<th>&nbsp;</th>
															</tr>
														</thead>
														<tbody>
															{docs?.map((el, i) => (
																<tr key={`element${i}`} >
																	<td><div className="pxp-company-dashboard-subscriptions-plan">{el.name}</div></td>
																	<td>
																		<div className="pxp-dashboard-table-options">
																			<ul className="list-unstyled">
																				<li>
																					<a  href={`${process.env.REACT_APP_API_STATIC}/static/docs/${el.path}`}  title="Télécharger"><span className="fa fa-download"></span></a>
																					</li>
																			</ul>
																		</div>
																	</td>
																</tr>
																))}
														</tbody>
													</table>
												</div>

												<div className="table-responsive">
													<table className="table table-hover align-middle">
														<thead>
															<tr>
																<th >Formulaires</th>
																<th>&nbsp;</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><div className="pxp-company-dashboard-subscriptions-plan">Demande d'evaluation</div></td>
																<td>
																	<div className="pxp-dashboard-table-options">
																		<ul className="list-unstyled">
																			<li><button title="Consulter" onClick={()=>{navigate('/evaluation-request')}}><span className="fa fa-external-link "></span></button></li>
																		</ul>
																	</div>
																</td>
															</tr>
															<tr>
																<td><div className="pxp-company-dashboard-subscriptions-plan">Nouvelle offre d'emploi</div></td>
																<td>
																	<div className="pxp-dashboard-table-options">
																		<ul className="list-unstyled">
																			<li><button title="Consulter" onClick={()=>{navigate('/add-offre')}} ><span className="fa fa-external-link "></span></button></li>
																		</ul>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div className="table-responsive">
													<table className="table table-hover align-middle">
														<thead>
															<tr>
																<th>   Contrat entreprise  </th>
																<th>    Consulter           </th>
																<th>    Chnager Statut		</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td><div className="pxp-company-dashboard-subscriptions-plan">Contract du l'entreprise  </div></td>
																<td>
																	{
																		noContractFound ? 
																			<div className="pxp-company-dashboard-subscriptions">
																				Pas Encore uploadé
																			</div>
																		:
																			<div className="pxp-company-dashboard-subscriptions-plan"> 
																					<button className="btn btn-success" style={{marginLeft:'12px'}} onClick={downloadEmployerContract} >
																					<span className="fa fa-download"></span>																					</button>
																			</div>
																	}
																	
																</td>
																<td>
																	{
																	noContractFound ? 
																		<div className="pxp-company-dashboard-subscriptions"></div>
																		:
																		<div>
																			<div className="form-check form-switch" style={{marginLeft:'50px'}} >
																				<input className="form-check-input" type="checkbox" id="flexSwitchCheckChecked" />
																			</div>
																			<label class="form-check-label" for="flexSwitchCheckDefault">Valider/Rejeter</label>
																		</div>
																	}
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												</div>
											</div>
											<Comment employerId={id} userId={auth.id} userComments={userComments} comments={comments}/>
											</div>
									</div>
								</div>
								<Log data={logs} userRole ={user.role} />
						</div>)
		   }
		</>
	);
}
