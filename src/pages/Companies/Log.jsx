export default function Log({data , userRole}){
    if (userRole === 'Admin') {
        return (
          <div className="pxp-dashboard-content-details">
            <h1>Log</h1>
            <p className="pxp-text-light">Historiques des actions</p>
            <div className="mt-4 mt-lg-5">
              <div className="table-responsive">
                <table className="table table-hover align-middle">
                  <tbody>
                    {data?.map((log) => {
                      return (
                        <tr key={log.id}> 
                          <td>
                            <div className="pxp-dashboard-notifications-item-left">
                              <div className="pxp-dashboard-notifications-item-type">
                                <span className="fa fa-briefcase"></span>
                              </div>
                              <div className="pxp-dashboard-notifications-item-message">
                                {log.action}
                              </div>
                            </div>
                          </td>
                          <td>
                            <div className="pxp-dashboard-notifications-item-right">
                              {log.createdAt}
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        );
      }  
}