import React, { useState, useEffect,useContext } from "react";
import { AuthContext } from "../../context/Auth";
import { instance } from "../../lib/axios";
import { CompaniesListBodyTable } from "../../components/table/CompaniesListBodyTable";
import Pagination from "../../components/pagination/Pagination";

export default function CompaniesListe() {
	const [companies, setCompanies] = useState([]);
	const [allEmployers, setAllEmployers] = useState([]);
	const [loading, setLoading] = useState(true);
	const [currentPage, setCurrentPage] = useState(1);
	const [totale,setTotale] = useState(0);
	const [user, setUser]= useState({})
    const { auth } = useContext(AuthContext);
	useEffect(() => {
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				const data = res.data.results ;
				setUser(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);
	const Search = (e) => {
		const filteredCompanies = allEmployers.filter(({ companyName, contactName,contactPhone,email }) =>
		companyName.toLowerCase().includes(e.toLowerCase()) || contactName.toLowerCase().includes(e.toLowerCase())
		|| contactPhone.toLowerCase().includes(e.toLowerCase()) 
		|| email.toLowerCase().includes(e.toLowerCase()) 
		);
		setCompanies(filteredCompanies);
   };
	function isFloat(value) {
		if (
		  typeof value === 'number' &&
		  !Number.isNaN(value) &&
		  !Number.isInteger(value)
		) {
		  return true;
		}
		return false;
	  }
	useEffect(()=>{
		const skip = 0;
        const take = 15;
		instance.get(`/employers?skip=${skip}&take=${take}`)
		.then((res)=>{
			setCompanies(res.data.results.allemployers)
			setTotale(res.data.results.employersCount)
			setAllEmployers(res.data.results.allemployers)
			setLoading(false)
				})

    }, [])
	useEffect(()=>{
        const skip = (currentPage - 1) * 15 ;
        const take = 15;
        instance.get(`/employers?skip=${skip}&take=${take}`).then((response)=>{
			setCompanies(response.data.results.employers) 
			setTotale(response.data.results.allemployers.length)
			setLoading(false) 
		})  
    }, [currentPage])
	const handlePageChange = (newPage) => {
		setCurrentPage(newPage);
	  };
	return (
		<>
		{
			loading? (
				<div className="pxp-dashboard-content-details">
				<div className="text-center">
					<div className="spinner-border" role="status">
						<span className="sr-only">Loading...</span>
					</div>
				</div>
		</div>	
						) : (
							companies.length === 0 ? 
							<div className="pxp-dashboard-content-details">
								<div style={{textAlign:'center'}}>
									<span>Aucune entreprise  Pour le moment</span>
								</div>
							</div>
							: 
							<div className="pxp-dashboard-content-details">
							<div className="mt-4 mt-lg-5">
								<div className="row justify-content-between align-content-center">
									<div className="col-auto order-2 order-sm-1">
										<div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
											<h1>Les entreprises</h1>                               
										</div>
									</div>
									<div className="col-auto order-1 order-sm-2">
										<div className="pxp-candidate-dashboard-jobs-search mb-3">
											<div className="pxp-candidate-dashboard-jobs-search-results me-3">{totale} entreprises</div>
											<div className="pxp-candidate-dashboard-jobs-search-search-form">
												<div className="input-group">
													<span className="input-group-text"><span className="fa fa-search"></span></span>
													<input type="text" className="form-control" placeholder="Recherche entreprise..."  onChange={({ target }) => Search(target.value)}/>
												</div>
											</div>
										</div>
									</div>    
								</div> 
							</div>    
							<div className="table-responsive">
								<table className="table table-hover align-middle">
									<thead>
											<tr>  
												<th>Entrprise</th>
												<th>Contact</th>
												<th >Email</th>
												{
													user.role ==='Admin' &&
													<th >Affectation</th>
												}
												<th >Téléphone</th>								
												<th>Status</th>
												<th>Date d'inscription	</th>
												<th>Actions</th>
			
											</tr>
										</thead>
									<CompaniesListBodyTable data={companies} />
								</table>
								<Pagination currentPage={currentPage} totalPages={isFloat(totale/15) ? Math.floor(totale/15) +1  : (totale/15)} onPageChange={handlePageChange}/>
						</div>
						</div>
			
						)
		}
		</>
	);
}
