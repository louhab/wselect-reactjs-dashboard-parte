import React , {useEffect,useState} from 'react';
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";

export default function Comment({ userId ,employerId ,userComments,comments}){
    const [users,setUsers] = useState([]);
    const [comment,setComment] = useState('');
    useEffect(()=>{
        instance.get(`/users?skip=0&take=14`).then((res)=>{
            setUsers(res.data.results.allusers);
        })
    }, [])
    async function handlAddComment(e) {
        e.preventDefault();
       const data = {
        employerId:employerId,
        comment:comment
       }
       instance.post(`/comments`,data)
       toast.success("Bravo!,le commentaire a été ajouté, Merci");
       setTimeout(() => {
        window.location.reload(false);
        }, 500);      
    }
    return(
     	 <div className="col-6">
				<div className="pxp-dashboard-inbox-messages-container">
                    <div className="pxp-dashboard-inbox-messages-header">
                        <div className="pxp-dashboard-inbox-list-item-left">
                            <div className="pxp-dashboard-inbox-list-item-name">Commentaires</div>
                        </div>
                    </div>
                    <div className="pxp-dashboard-inbox-messages-content">
                    <Toaster position="bottom-center" reverseOrder={false} />

                        {
                            comments.map(comment=>{
                                return(
                                    <div className="pxp-dashboard-inbox-messages-item mt-4">
                                    <div className="row">
                                        <div className="col-7">
                                            <div className="pxp-dashboard-inbox-messages-item-header">
                                                <div className="pxp-dashboard-inbox-messages-item-name ms-2">
                                                    {users.find((user) => user.id === comment.userId)?.role}
                                            </div>
                                                <div className="pxp-dashboard-inbox-messages-item-time pxp-text-light ms-3">{comment.createdAt }</div>
                                            </div>
                                            <div className="pxp-dashboard-inbox-messages-item-message pxp-is-self mt-2">
                                                {comment.comment}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                )
                            })
                        }
                     {
                        userComments.map((comment) => {
                            return (
                                <div className="pxp-dashboard-inbox-messages-item">
                                <div className="row justify-content-end">
                                    <div className="col-7">
                                        <div className="pxp-dashboard-inbox-messages-item-header flex-row-reverse">
                                            <div className="pxp-dashboard-inbox-messages-item-name me-2">{
                                                users.find((user) => user.id === comment.userId)?.role
                                            }</div>
                                            <div className="pxp-dashboard-inbox-messages-item-time pxp-text-light me-3">{comment.createdAt}</div>
                                        </div>
                                        <div className="pxp-dashboard-inbox-messages-item-message pxp-is-other mt-2">
                                           {comment.comment}
                                        </div>
                           </div>
                           </div>
                           </div>
                            )
                        })
                    }   
                    </div>
                    <form onSubmit={handlAddComment}>
                    <div className="pxp-dashboard-inbox-messages-footer">
                      <div className="pxp-dashboard-inbox-messages-footer-field">
                            <input type="text" className="form-control" placeholder="Type your message here..." onChange={(e)=>{setComment(e.target.value)}} />
                        </div>
                        <div className="pxp-dashboard-inbox-messages-footer-btn">
                            <button className="btn rounded-pill pxp-section-cta"  style={{fontSize:'12px'}} >Envoyer</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
)
}