import React from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useForm } from 'react-hook-form';
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useNavigate } from "react-router-dom";
export default function AddUsers() {
    const navigate = useNavigate();
    const schema = Joi.object({
        fullname:Joi.string().max(200).required().label("fullname").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        role:Joi.string().required().label('role').messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        email:Joi.string().min(5).max(200).required().label("email").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        phone:Joi.string().min(5).max(200).required().label("phone")
        .messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        password:Joi.string().max(200).required().label("password")
        .messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        passwordConfirmation: Joi.string().valid(Joi.ref('password')).required()
        .messages({
			"string.empty": "Ce champ est obligatoire",
            "any.only": 'Les mots de passe doivent correspondre', 
		}),
    })
    const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});
    async function onSubmit(values) {
    delete values.passwordConfirmation;
	try {
        await instance.post("/users", values);
        toast.success("Bravo!,l'utilisateur est ajouté");
        setTimeout(() => {
                 navigate("/users", { replace: true });
        }, 500);
    }
     catch(e){
            console.log(e)
    }
	}
return (
		<>
   		<div className="pxp-dashboard-content-details">
				<Toaster position="bottom-center" reverseOrder={false} />
                <h1>Ajouter utilisateur</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row mt-4 mt-lg-5">
                        <div className="col-xxl-12">
                            <div className="mb-3">
                                <label htmlFor="pxp-company-name" className="form-label">Nom de l'utilisateur</label>
                                <input style={{
											border:
												errors["fullname"] &&
												errors["fullname"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
										      }}
                                         {...register('fullname')} type="text" id="pxp-company-name" className="form-control" placeholder="Nom de l'utilisateur"/>
                                {errors && errors.fullname && <p className="text-danger">{errors.fullname.message}</p>}

                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-company-size" className="form-label">Role</label>
                                        <select
                                        placeholder="Role"
                                        style={{
                                            border:
                                                errors["role"] &&
                                                errors["role"].message
                                                    ? "1px solid rgba(255, 0, 0, 0.3)"
                                                    : null, 
                                            }}
                                         {...register('role')} id="pxp-company-size" className="form-select"    
                                    >
                                        <option value="" disabled selected>Sélectionner le role </option>
                                        <option value="Admin">Admin</option>
                                        <option value="Commercial">Commercial</option>
							        </select>
							        {errors && errors.role && <p className="text-danger">{errors.role.message}</p>}
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-company-email" className="form-label">Email</label>
                                        <input 
                                        placeholder="Email du l'utilisateur"
                                        style={{
											border:
												errors["email"] &&
												errors["email"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
										      }}
                                        {...register('email')}    type="email" id="pxp-company-email" className="form-control" />
                                        {errors && errors.email && <p className="text-danger">{errors.email.message}</p>}
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-company-phone" className="form-label">Téléphone</label>
                                        <input 
                                        style={{
											border:
												errors["phone"] &&
												errors["phone"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
										      }}
                                              placeholder="Téléphone..."

                                        {...register('phone')}   type="text" id="pxp-company-phone" className="form-control" />
                                        {errors && errors.phone && <p className="text-danger">{errors.phone.message}</p>}
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-candidate-new-password" className="form-label">Mot de passe</label>
                                        <input 
                                            style={{
											border:
												errors["password"] &&
												errors["password"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
										      }}
                                              placeholder="Mot de passe ..."
                                        {...register('password')}   type="password" id="pxp-candidate-new-password" className="form-control"  />
                                        {errors && errors.password && <p className="text-danger">{errors.password.message}</p>}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-candidate-new-password-repeat" className="form-label">Répéter mot de passe</label>
                                        <input
                                        style={{
											border:
												errors["passwordConfirmation"] &&
												errors["passwordConfirmation"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
										      }}
                                              placeholder="Répeter le mot de passe  ..."
                                        {...register('passwordConfirmation')} type="password" id="pxp-candidate-new-password-repeat" className="form-control"  />
                                        {errors && errors.passwordConfirmation && <p className="text-danger">{errors.passwordConfirmation.message}</p>}
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div className="mt-4 mt-lg-5">
                        <button  className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
                    </div>
                </form>
            </div>
		</>
	);
}
