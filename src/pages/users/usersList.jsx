import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import { Link } from "react-router-dom";
import { UsersBodyTable } from "../../components/table/UsersBodyTable";
import Pagination from "../../components/pagination/Pagination";
import classNames from "classnames";
export default function UsersListe() {
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [totale, setTotale] = useState(0);
    const [allUsers, setAllUsers] = useState([])
    const handlePageChange = (newPage) => {
        setCurrentPage(newPage);
    };
   const Search = (e) => {
    const searchedUsers = allUsers.filter(({ fullname, phone,email }) =>
    fullname.toLowerCase().includes(e.toLowerCase()) || phone.toLowerCase().includes(e.toLowerCase())
    || email.toLowerCase().includes(e.toLowerCase()) 
    );
    setUsers(searchedUsers) ;
};
    useEffect(() => {
        const skip = (currentPage - 1) * 15;
        const take = 15;
        instance.get(`/users?skip=${skip}&take=${take}`).then((response) => {
            setUsers(response.data.results.users);
            setAllUsers(response.data.results.allusers)
            setTotale(response.data.results.allusers.length);
            setLoading(false);
        });
    }, [currentPage]);
return (
        <div className="pxp-dashboard-content-details">
            {loading ? (
                <div className="text-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            ) : (
                <>
                {
                    totale === 0 ?
                    <div >
                    <div className="col-auto order-2 order-sm-1">
                             <div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
                                 <h1>Utilisateurs</h1>
                                 <Link
                                     to={"/add-users"}
                                     className="btn ms-2"
                                 >
                                    Nouveau utilisateur
                                 </Link>
                             </div>
                     </div>
                     <div style={{textAlign:'center'}}>
                          <span>Aucun utilisateur Pour le moment</span>
                     </div>
             </div>
                    :
                   <div>
                     <div className={classNames("mt-4", "mt-lg-5")}>
                    <div className="row justify-content-between align-content-center">
                        <div className={classNames("col-auto", "order-2", "order-sm-1")}>
                            <div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
                                <h1>Utilisateurs</h1>
                                <Link to={"/add-users"} className="btn ms-2">
                                    Nouveau utilisateur
                                </Link>
                            </div>
                        </div>
                        <div className={classNames("col-auto", "order-1", "order-sm-2")}>
                            <div className="pxp-candidate-dashboard-jobs-search mb-3">
                                <div className="pxp-candidate-dashboard-jobs-search-results me-3">
                                    {totale} users
                                </div>
                                <div className="pxp-candidate-dashboard-jobs-search-search-form">
                                    <div className="input-group">
                                        <span className="input-group-text">
                                            <span className="fa fa-search"></span>
                                        </span>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Recherche de l'offre..."
                                            onChange={({ target }) => Search(target.value)}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover align-middle">
                            <thead>
                                <tr>
                                    <th>Nom</th>
                                    <th>Login</th>
                                    <th>Téléphone</th>
                                    <th>Role</th>
                                    <th>Date d'inscription</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <UsersBodyTable data={users} />
                        </table>
                        <Pagination currentPage={currentPage} totalPages={Math.ceil(totale / 15)} onPageChange={handlePageChange} />
                    </div>
                   </div>
                }
                </>
            )}
        </div>
    );
}
