import React, {useState,useEffect} from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useForm } from 'react-hook-form';
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import bcrypt from 'bcryptjs'
import { useLocation} from "react-router-dom";
function LoadingSpinner() {
    return (
        <div className="pxp-dashboard-content-details">
            <div className="text-center">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    );
}
export default function AddUsers() {
    const [error, setError] = useState(false)
    const [formData, setFormData] = useState({
        fullname: '',
        email: '',
        phone: '',
        password: '',
        passwordConfirmation: ''
    });
    const schema = Joi.object({
        fullname:Joi.string().max(200).required().label("fullname").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        email:Joi.string().min(5).max(200).required().label("email").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        password:Joi.string().max(200).required().label("password")
        .messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        passwordConfirmation: Joi.string().valid(Joi.ref('password')).required()
        .messages({
			"string.empty": "Ce champ est obligatoire",
            "any.only": 'Les mots de passe doivent correspondre', 
		}),
    })
    const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});
    const location = useLocation();
    const userId = location.pathname.replace(/^\D+/g, '');
    useEffect(() => {
        instance.get(`/users/${userId}`)
        .then((response)=>{
            setFormData({
                fullname: response.data.results.fullname,
                email: response.data.results.email,
                phone: response.data.results.phone,
                password: response.data.results.password
            });
        })
        .catch((error)=>{
            setError(true)
            console.log(error)
        })

	}, []);
    async function onSubmit(values) {
    delete values.passwordConfirmation;
	try {
        const hashedPassword = bcrypt.hashSync(values.password, '$2a$10$CwTycUXWue0Thq9StjUM0u');
        const updatedUser = { ...values, password: hashedPassword };
        await instance.put(`/users/${userId}`, updatedUser);
        toast.success("Toutes nos félicitations! votre profil a été mis a jour");
        window.location.reload();
    }
        catch(e){
            toast.error("Une erreur a été produite");
            console.log(e)
        }
	}
    const handleFullnameChange = (event) => {
        const updatedFormData = { ...formData, fullname: event.target.value };
        setFormData(updatedFormData);
      };
    const handleEmailChange = (event) => {
        const updatedFormData = { ...formData, email: event.target.value };
        setFormData(updatedFormData);
    };
return (
		<>
   		{
            error ? 
            <LoadingSpinner/>
            :
            <div className="pxp-dashboard-content-details">
            <Toaster position="bottom-center" reverseOrder={false} />
                <h1>Modifier votre profil</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row mt-4 mt-lg-5">
                        <div className="col-xxl-12">
                            <div className="mb-3">
                                <label htmlFor="pxp-company-name" className="form-label">Nom et le Prenom</label>
                                <input style={{
                                            border:
                                                errors["fullname"] &&
                                                errors["fullname"].message
                                                    ? "1px solid rgba(255, 0, 0, 0.3)"
                                                    : null, 
                                                }}
                                            {...register('fullname')} type="text" id="pxp-company-name" className="form-control" value={formData.fullname} onChange={handleFullnameChange}/>
                                {errors && errors.fullname && <p className="text-danger">{errors.fullname.message}</p>}
                            </div>
                            <div className="mb-3">
                                <label htmlFor="pxp-company-name" className="form-label">Email</label>
                                <input style={{
                                            border:
                                                errors["email"] &&
                                                errors["email"].message
                                                    ? "1px solid rgba(255, 0, 0, 0.3)"
                                                    : null, 
                                                }}
                                            {...register('email')} type="email" id="pxp-company-name" className="form-control" value={formData.email}   onChange={handleEmailChange} />
                                {errors && errors.email && <p className="text-danger">{errors.email.message}</p>}
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-candidate-new-password" className="form-label">Mot de passe</label>
                                        <input 
                                        style={{
                                            border:
                                                errors["password"] &&
                                                errors["password"].message
                                                    ? "1px solid rgba(255, 0, 0, 0.3)"
                                                    : null, 
                                                }}
                                        {...register('password')}   type="password" id="pxp-candidate-new-password" className="form-control" placeholder="" />
                                        {errors && errors.password && <p className="text-danger">{errors.password.message}</p>}
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-candidate-new-password-repeat" className="form-label">Répéter mot de passe</label>
                                        <input
                                        style={{
                                            border:
                                                errors["passwordConfirmation"] &&
                                                errors["passwordConfirmation"].message
                                                    ? "1px solid rgba(255, 0, 0, 0.3)"
                                                    : null, 
                                                }}
                                        {...register('passwordConfirmation')} type="password" id="pxp-candidate-new-password-repeat" className="form-control" placeholder="" />
                                        {errors && errors.passwordConfirmation && <p className="text-danger">{errors.passwordConfirmation.message}</p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mt-4 mt-lg-5">
                        <button  className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
                    </div>
                </form>
            </div>
        }
		</>
	);
}
