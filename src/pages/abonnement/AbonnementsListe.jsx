import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import { AbonnementsBodyTable } from "../../components/table/AbonnementsBodyTable";
import Pagination from '../../components/pagination/Pagination';
function LoadingSpinner() {
    return (
        <div className="pxp-dashboard-content-details">
            <div className="text-center">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    );
}
export default function AbonnementsListe() {
	const [abonnements, setAbonnements] = useState([]);
	const [allAbonnement, setAllAbonnement] = useState("");
	const [loading, setLoading] = useState(true);
	const [currentPage, setCurrentPage] = useState(1);
	const [totale,setTotale] = useState(0); 
	const Search = (e) => {
	const filteredAbonnement = allAbonnement.filter(({ title }) =>
	title.toLowerCase().includes(e.toLowerCase()) 
	);
	setAbonnements(filteredAbonnement);
   };
    useEffect(()=>{
		const skip = 0;
        const take = 15;
		instance.get(`/abonnements?skip=${skip}&take=${take}`)
		.then((res)=>{
			setAbonnements(res.data.results.allAbonnements)
			setTotale(res.data.results.allAbonnements.length)
			setLoading(false)
				})
    }, [])
	useEffect(()=>{
        const skip = (currentPage - 1) * 15;
        const take = 15;
        instance.get(`/abonnements?skip=${skip}&take=${take}`).then((response)=>{
			setAbonnements(response.data.results.abonnements) 
			setTotale(response.data.results.allAbonnements.length)
			setAllAbonnement(response.data.results.allAbonnements)
			setLoading(false) 
		})  
    }, [currentPage])
	const handlePageChange = (newPage) => {
		setCurrentPage(newPage);
	  };

	return (
		<>
		{
			loading?    (
						<LoadingSpinner />					
						) :
						(
						totale === 0 ?
						<div className="pxp-dashboard-content-details">
							<div style={{textAlign:'center'}}>
								<span>Aucun abonnement  Pour le moment</span>
							</div>
						</div>
						:	
						<div className="pxp-dashboard-content-details">
							<div className="mt-4 mt-lg-5">
								<div className="row justify-content-between align-content-center">
									<div className="col-auto order-2 order-sm-1">
										<div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
										<h1>Abonnements</h1>                               
									</div>
								</div>
									<div className="col-auto order-1 order-sm-2">
										<div className="pxp-candidate-dashboard-jobs-search mb-3">
											<div className="pxp-candidate-dashboard-jobs-search-results me-3">{totale} Abonnements</div>
											<div className="pxp-candidate-dashboard-jobs-search-search-form">
												<div className="input-group">
													<span className="input-group-text"><span className="fa fa-search"></span></span>
													<input type="text" className="form-control" placeholder="Recherche de candidat..." onChange={({ target }) => Search(target.value)}/>
												</div>
											</div>
										</div>
									</div>    
								</div> 
							</div>    
							<div className="table-responsive">
								<table className="table table-hover align-middle">
									<thead>
									<tr>
										<th >Candidate</th>
										<th >Pack </th>
										<th >Status</th>
										<th >Date d'expiration</th>
										<th>&nbsp;</th>
										</tr>       
									</thead>
									<AbonnementsBodyTable data={abonnements} />
								</table>
								<Pagination currentPage={currentPage} totalPages={Math.ceil(totale / 15)} onPageChange={handlePageChange}/>
							</div>	
						</div>
						)
					}
		</>
	);
}
