import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import { useLocation } from "react-router-dom";
import {CandidatesOnOfferTable } from "../../components/table/CandidatesOnOfferTable"
export default function CandidatesOffer() {
	const location = useLocation();
	let id = location.pathname.replace(/^\D+/g, '')
	const [condidates, setCondidates] = useState([]);
	const [loading, setLoading] = useState(true);
	const [offre, setOffre]= useState({});
    const fetchCandidatures = async() =>{
        const {data} = await instance.get(`/offers/${id}`); 
		setCondidates(data.results.PreCandidate) 
		setOffre(data.results)
		setLoading(false)
    }
    useEffect(()=>{
        fetchCandidatures()   
    }, [])
	return (
		<>
		{
			loading? (
						<div className="pxp-dashboard-content-details">
								<div className="text-center">
									<div className="spinner-border" role="status">
										<span className="sr-only">Loading...</span>
									</div>
								</div>
						</div>		
						) : (
							<div className="pxp-dashboard-content-details">
				<div className="mt-4 mt-lg-5">
					<div className="row justify-content-between align-content-center">
						<div className="col-auto order-2 order-sm-1">
							<div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
							<span className="badge bg-primary"> {offre.title}</span>
						</div>
						</div>
					</div> 
				</div>    
				<div className="table-responsive">
					<table className="table table-hover align-middle">
						<thead>
                                <tr>
                                    
                                    <th>Nom</th>
                                    <th>Téléphone</th>
                                    <th >Status</th>
                                    <th>Date d'inscription</th>
									<th>&nbsp;</th>

                                </tr>
                        	</thead>
						<CandidatesOnOfferTable   data={condidates} />
					</table>
			</div>
			</div>)}
		</>
	);
}
