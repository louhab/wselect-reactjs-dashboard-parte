import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import { useLocation } from 'react-router-dom';

export default function DetailsCandidate() {
	const [candidate, setCandidate] = useState([])
	const location = useLocation();
	const id = location.pathname.charAt(19);
    const fetchCandidature = async() =>{
        const {data} = await instance.get(`/candidates/${id}`); 
		setCandidate(data.results)
    }
    useEffect(()=>{
        fetchCandidature()   
    }, [])
	return (
		<>
			<div className="pxp-dashboard-content-details"> 
			<h1 className="mx-auto">Candidature </h1>
				<div className="row">
					<div className="col-9">
						<div className="mt-3 mt-lg-4">
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Nom et prénom  du candidat</div>
								<div className="pxp-single-job-side-info-data">{candidate.firstName} {candidate.lastName} </div>
							</div>
							
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Email du candidat </div>
								<div className="pxp-single-job-side-info-data">{candidate.email}</div>
							</div>
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Téléphone du candidat</div>
								<div className="pxp-single-job-side-info-data">{candidate.phone}</div>
							</div>
							
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Ville du candidat</div>
								<div className="pxp-single-job-side-info-data">Casablanca</div>
							</div>

							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Domaine d'activite</div>
								<div className="pxp-single-job-side-info-data">{candidate.diplomasName}</div>
							</div>

							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Diplome du candidat </div>
								<div className="pxp-single-job-side-info-data">{candidate.diplomasName}</div>
							</div>
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">CV du candidat </div>
								<div className="pxp-single-job-side-info-data">  
									<a  href={`${process.env.REACT_APP_API_STATIC}/static/${candidate.cvPath}`}  title="Télécharger"><span className="fa fa-download"></span>Télécharger</a>
								</div>
							</div>
							
						</div>
					</div>	
				</div>	
			</div>
	
		</>
	);
}
