import React,{ useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";


export default function OfferDetail() {
	const [companies, setCompanies] = useState([]);
	const [name, setName] =useState('');
	const [diplomas, setDiplomas] = useState('');
	const [company, setCompany] = useState(1);
	const [description, setDescription] = useState('');
	const [requirement , setRequirement] = useState('');
	const [MinExpYears,setMinExpYears]= useState('one');
	const [MinLanguageLevel,setMinLanguageLevel]= useState('one');

	const fetchCompanies = async() =>{
        const {data} = await instance.get(`/companies`); 
        setCompanies(data.results) 
    }
    useEffect(()=>{
        fetchCompanies()   
    }, [])
	async function handlRequirements(requirement){
		if(requirement==='OUI'){
			setRequirement(true)
		}
		else {
			setRequirement(false)
		}	
	}
	async function handleSubmit(e) {
		try {
			e.preventDefault();
			let form = {
				title: name,
				diplomasName: diplomas,
				isDiplomasRequired:requirement,
				campaignId: company,
				minExpYears:MinExpYears,
				minLanguageLevel:MinLanguageLevel,

			}
		  console.log(form)
			await instance.post("/offers", form);
			toast.success("Bravo!,l'offer est ajouté");
			 setTimeout(() => {
				window.location.reload(false);
			}, 500);
		} catch (e) {
			toast.error("une erreur a été produite");
			console.log(e);
		}
	}

	return (
		<>
    <div className="pxp-dashboard-content-details">
	<Toaster position="bottom-center" reverseOrder={false} />
                <h1>Ajouter une nouvelle  offre </h1>
                <form  onSubmit={handleSubmit}>
                    <div className="row mt-4 mt-lg-5">
                        <div className="col-xxl-6">
                            <div className="mb-3">
                                <label htmlFor="pxp-company-name" className="form-label">Nom de l'offre</label>
                                <input  onChange={(e)=>{setName(e.target.value)}} type="text" id="pxp-company-name" className="form-control" placeholder="Nom de l'offre"/>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-company-size" className="form-label">Entreprises</label>
                                        <select id="pxp-company-size" className="form-select"  onChange={(e)=>{setCompany(e.target.value)}}>
										<option value=""></option>
											{
												companies.map(company => (
												<option key={company.id} value={company.id}>{company.name}</option>
											))
											}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div className="col-xxl-4">
							<label htmlFor="pxp-company-name" className="form-label">Nom du diplome</label>
							<input  onChange={(e)=>{setDiplomas(e.target.value)}} type="text" id="pxp-company-name" className="form-control" placeholder="Nom du diplome"/>
						</div>
						<div className="col-xxl-2">
							<label htmlFor="pxp-company-name" className="form-label">Obligatoire ? </label>
							<select  id="pxp-company-size" className="form-select"
							 onChange={(e)=>handlRequirements(e.target.value)}
							>
								<option value=""></option>
								<option value="Oui">Oui</option>
								<option value="Non">Non</option>
							</select>
						</div>
                    </div>
					<div className="row">
						<div className="col-xxl-6">
						<label htmlFor="pxp-company-name" className="form-label">Expérience minimale</label>
							<select  id="pxp-company-size" className="form-select"
							 onChange={(e)=>{setMinExpYears(e.target.value)}}
							>
								<option value=""></option>
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
							</select>
						</div>
						<div className="col-xxl-6">
							<label htmlFor="pxp-company-name" className="form-label">Niveau du langue minimale </label>
							<select  id="pxp-company-size" className="form-select"
							 onChange={(e)=>{setMinLanguageLevel(e.target.value)}}
							>
								<option value=""></option>
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
							</select>
						</div>
					</div>
                    <div className="mb-3">
                        <label htmlFor="pxp-company-about" className="form-label">Description de l'offre</label>
                        <textarea className="form-control" 	onChange={(e)=>{setDescription(e.target.value)}} id="pxp-company-about" placeholder=""></textarea>
                    </div>
                    <div className="mt-4 mt-lg-5">
                        <button className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
                    </div>
                </form>
            </div>
		</>
	);
}
