import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import { Link } from "react-router-dom";
import { OffersBodyTable } from "../../components/table/OffersBodyTable";
import Pagination from '../../components/pagination/Pagination';
function LoadingSpinner() {
    return (
        <div className="pxp-dashboard-content-details">
            <div className="text-center">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    );
}
function SearchInput({ onChange }) {
    return (
        <div className="input-group">
            <span className="input-group-text"><span className="fa fa-search"></span></span>
            <input type="text" className="form-control" placeholder="Recherche de l'offre..." onChange={onChange} />
        </div>
    );
}
function NoDataFound(){
	<div className="pxp-dashboard-content-details">
		<span>Aucune Enregistrement Pour le moment </span>
	</div>
}
export default function OffersList() {
    const [offers, setOffers] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalOffers, setTotalOffers] = useState(0);
	const [allOffers , setAllOffers] = useState([]);
    const fetchOffers = async () => {
        const skip = (currentPage - 1) * 15;
        const take = 15;
        const { data } = await instance.get(`/offers?skip=${skip}&take=${take}`);
        setTotalOffers(data.results.offerCount);
        setOffers(data.results.offers);
		setAllOffers(data.results.allOffers)
        setLoading(false);
    };
    useEffect(() => {
        fetchOffers();
    }, [currentPage]);
    const handlePageChange = (newPage) => {
        setCurrentPage(newPage);
    };
	const search = (e) => {
		 const searchedOffers = allOffers.filter(({ title }) =>title.includes(e));
		 setOffers(searchedOffers)
    };

return (
        <>
            {loading ? <LoadingSpinner /> : 
            (
               totalOffers === 0 ?
               <div className="pxp-dashboard-content-details">
                       <div className="col-auto order-2 order-sm-1">
                                <div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
                                    <h1>Offres</h1>
                                    <Link
                                        to={"/add-offre"}
                                        className="btn ms-2"
                                    >
                                        Nouvelle Offre
                                    </Link>
                                </div>
                        </div>
                        <div style={{textAlign:'center'}}>
                             <span>Aucun offre Pour le moment</span>
                        </div>
                </div>
               :
                <div className="pxp-dashboard-content-details">
                    <div className="mt-4 mt-lg-5">
                        <div className="row justify-content-between align-content-center">
                            <div className="col-auto order-2 order-sm-1">
                                <div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
                                    <h1>Offres</h1>
                                    <Link
                                        to={"/add-offre"}
                                        className="btn ms-2"
                                    >
                                        Nouvelle Offre
                                    </Link>
                                </div>
                            </div>
                            <div className="col-auto order-1 order-sm-2">
                                <div className="pxp-candidate-dashboard-jobs-search mb-3">
                                    <div className="pxp-candidate-dashboard-jobs-search-results me-3">{totalOffers} Offres</div>
                                    <div className="pxp-candidate-dashboard-jobs-search-search-form">
                                        <SearchInput onChange={({ target }) => search(target.value)} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-hover align-middle">
                            <thead>
                                <tr>
                                    <th>Offre</th>
                                    <th>Entreprise</th>
                                    <th>Nombre candidates</th>
                                    <th>Date de création</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <OffersBodyTable data={offers} />
                        </table>
                        <Pagination currentPage={currentPage} totalPages={Math.ceil(totalOffers / 15)} onPageChange={handlePageChange} />
                    </div>
                </div>
            )}
        </>
    );
}
