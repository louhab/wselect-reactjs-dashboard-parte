import React,{ useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";


export default function OfferDetails() {
	const [companies, setCompanies] = useState([]);
	const [name, setName] =useState('');
	const [company, setCompany] = useState('');
	const [description, setDescription] = useState('');

	const fetchCompanies = async() =>{
        const {data} = await instance.get(`/companies`); 
        setCompanies(data.results) 
    }
    useEffect(()=>{
        fetchCompanies()   
    }, [])

	async function handleSubmit(e) {
		try {
			e.preventDefault();
			let form = {
				title: name,
				campaignId: company,
				description: description,
			}
		  console.log(form)
			// await instance.post("/offers", form);
			// setTimeout(() => {
				toast.success("Bravo!");
			// }, 500);
			// setName('')
			// setCompany('')
            // setDescription('')
		} catch (e) {
			console.log(e);
		
		}
	}

	return (
		<>
    <div className="pxp-dashboard-content-details">
	<Toaster position="bottom-center" reverseOrder={false} />
                <h1>Ajouter une nouvelle  offre </h1>
                <form  onSubmit={handleSubmit}>
                    <div className="row mt-4 mt-lg-5">
                        <div className="col-xxl-8">
                            <div className="mb-3">
                                <label htmlFor="pxp-company-name" className="form-label">Nom de l'offre</label>
                                <input  onChange={(e)=>{setName(e.target.value)}} type="text" id="pxp-company-name" className="form-control" placeholder=""/>
                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="mb-3">
                                        <label htmlFor="pxp-company-size" className="form-label">Entreprises</label>
                                        <select id="pxp-company-size" className="form-select"  onChange={(e)=>{setCompany(e.target.value)}}>
											{
												companies.map(company => (
												<option key={company.id} value={company.id}>{company.name}</option>
											))
											}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mb-3">
                        <label for="pxp-company-about" className="form-label">Description de l'offre</label>
                        <textarea className="form-control" 	onChange={(e)=>{setDescription(e.target.value)}} id="pxp-company-about" placeholder=""></textarea>
                    </div>
                    <div className="mt-4 mt-lg-5">
                        <button className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
                    </div>
                </form>
            </div>
		</>
	);
}
