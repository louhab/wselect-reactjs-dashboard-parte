import React,{ useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useLocation } from 'react-router-dom';
import Joi from "joi";
import { useForm } from 'react-hook-form';
import { joiResolver } from "@hookform/resolvers/joi";

export default function EditOffer() {
	const location = useLocation();
	let id = location.pathname.replace(/^\D+/g, '')
	const [companies, setCompanies] = useState([]);
	const [formData, setFormData] = useState({
        title: '',
        diplomasName: '',
        company: '',
        description: '',
        isDiplomasRequired: '',
		minExpYears: '',
		employerId : '',
		MinLanguageLevel:'',

    });
	const schema = Joi.object({
        title:Joi.string().max(200).required().label("Nom du l'offre :").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        diplomasName:Joi.string().min(5).max(200).required().label("Nom du diplome :").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		isDiplomasRequired:Joi.string().max(200).required().label("obligatoire ? :")
        .messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        employerId:Joi.string().max(200).required().label("Le Nom du l'entreprise :")
        .messages({
			"string.empty": "Ce champ est obligatoire",
		}),
        description: Joi.string().required()
		.messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		minExpYears : Joi.string().required()
		.messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		minLanguageLevel : Joi.string().required()
		.messages({
			"string.empty": "Ce champ est obligatoire",
		}),
    })
	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});
	const fetchOffer = async()=>{
		const {data} = await instance.get(`/offers/${id}`); 
		setFormData({
            title: data.results.title,
            diplomasName: data.results.diplomasName,
            company: data.results.company,
            description: data.results.description,
			isDiplomasRequired: data.results.isDiplomasRequired,
			minLanguageLevel: data.results.minLanguageLevel,
			minExpYears:data.results.minExpYears,
			employerId : data.results.employer.id
        });
	}

	const fetchEmployers= async()=>{
		const {data} = await instance.get(`/employers?skip=0&take=100`);
		setCompanies(data.results.allemployers)

	}
	useEffect(()=>{
        fetchOffer() 
		fetchEmployers()
    }, [])
	async function onSubmit(values) {
		try {
			values.isDiplomasRequired = (values.isDiplomasRequired === "true")
			await instance.put(`/offers/${id}`, values);
			toast.success("Bravo!,l'offer est modifié");
			 setTimeout(() => {
				window.location.reload(false);
			}, 500);
			}
			catch(e){
			toast.error("une erreur a été produite");
			console.log(e);
			}
		}
	const handleDiplomasNameChange = (event) => {
	const updatedFormData = { ...formData, diplomasName: event.target.value };
	setFormData(updatedFormData);
	};
	const handleTitleChange = (event) => {
	const updatedFormData = { ...formData, title: event.target.value };
	setFormData(updatedFormData);	
	}
	const handleDescriptionChange = (event) => {
		const updatedFormData = { ...formData, description: event.target.value };
		setFormData(updatedFormData);
	}
	return (
		<>
			<div className="Edit-offer">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-dashboard-content-details">
				<h1 className="mx-auto">Modiifier l'offre</h1>
				<p className="pxp-text-light">Modifier les informations de l'offre</p>
				<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="col-xxl-6">
						<div className="mb-3">
							<label htmlFor="pxp-company-name" className="form-label">Nom de l'offre</label>
							<input
									{...register('title')}
									value={formData.title}
									onChange={handleTitleChange}
									type="text" id="pxp-company-name" 
									className="form-control" 
									style={{
										border:
											errors["title"] &&
											errors["title"].message
												? "1px solid rgba(255, 0, 0, 0.3)"
												: null, 
										  }}
									
							/>
							{errors && errors.title && <p className="text-danger">{errors.title.message}</p>}
						</div>
						<div className="row">
							<div className="col-sm-12">
								<div className="mb-3">
									<label htmlFor="pxp-company-size" className="form-label">Entreprises</label>
									<select 
									style={{
										border:
											errors["employerId"] &&
											errors["employerId"].message
												? "1px solid rgba(255, 0, 0, 0.3)"
												: null, 
									}}
									{...register('employerId')}
									id="pxp-company-size" className="form-select"  >
											<option value="" selected disabled>
												{companies.find(element => formData.employerId === element.id)?.companyName}		
											</option>
											{companies.map(company => (
												<option key={company.id} value={company.id}>{company.companyName}</option>
											))}
									</select>
									{errors && errors.employerId && <p className="text-danger">{errors.employerId.message}</p>}
								</div>
							</div>
						</div>
					</div>
					<div className="col-xxl-4">
						<label htmlFor="pxp-company-name" className="form-label">Nom du diplome</label>
						<input
						{...register('diplomasName')}
						onChange={handleDiplomasNameChange}
						value={formData.diplomasName}
						 type="text" 
						 id="pxp-company-name"
						 className="form-control"
						 style={{
							border:
								errors["diplomasName"] &&
								errors["diplomasName"].message
									? "1px solid rgba(255, 0, 0, 0.3)"
									: null, 
							  }}
						 placeholder={formData.diplomasName}
						 />
						{errors && errors.diplomasName && <p className="text-danger">{errors.diplomasName.message}</p>}

					</div>
					<div className="col-xxl-2">
						<label htmlFor="pxp-company-name" className="form-label">obligatoire  ? </label>
						<select 
						id="pxp-company-size" className="form-select"
								{...register('isDiplomasRequired')}
							 	style={{
								border:
									errors["isDiplomasRequired"] &&
									errors["isDiplomasRequired"].message
										? "1px solid rgba(255, 0, 0, 0.3)"
										: null, 
								}}>
									<option value="" disabled selected>
									{formData.isDiplomasRequired === true ? 'OUI' : 'NON'}
									</option>								
								<option value={true}>OUI</option>
								<option value={false}>NON</option>
						</select>
						{errors && errors.isDiplomasRequired && <p className="text-danger">{errors.isDiplomasRequired.message}</p>}
					</div>
				</div>
				<div className="row">
						<div className="col-xxl-6">
						<label htmlFor="pxp-company-name" className="form-label">Expérience minimale</label>
							<select  
								{...register('minExpYears')}
								style={{
									border:
										errors["minExpYears"] &&
										errors["minExpYears"].message
											? "1px solid rgba(255, 0, 0, 0.3)"
											: null, 
									}}
								id="pxp-company-size" 
								className="form-select">
								<option value="" disabled selected>
								{formData.minExpYears}
								</option>	
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
								<option value="three">four</option>
								<option value="three">five</option>
								<option value="six">six</option>
								<option value="seven">seven</option>
								<option value="eight">eight</option>
								<option value="nine">nine</option>
								<option value="ten">ten</option>
							</select>
							{errors && errors.minExpYears && <p className="text-danger">{errors.minExpYears.message}</p>}
						</div>
						<div className="col-xxl-6">
							<label htmlFor="pxp-company-name" className="form-label">Niveau du langue minimale </label>
							<select 
								{...register('minLanguageLevel')}
								style={{
									border:
										errors["minLanguageLevel"] &&
										errors["minLanguageLevel"].message
											? "1px solid rgba(255, 0, 0, 0.3)"
											: null, 
								}}
							
							id="pxp-company-size" className="form-select"
							 
							>
								<option value="" disabled selected>
								{formData.minLanguageLevel}
								</option>
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
								<option value="three">four</option>
								<option value="three">five</option>
								<option value="six">six</option>
								<option value="seven">seven</option>
								<option value="eight">eight</option>
								<option value="nine">nine</option>
								<option value="ten">ten</option>
							</select>
							{errors && errors.minLanguageLevel && <p className="text-danger">{errors.minLanguageLevel.message}</p>}
						</div>
				</div>
				<div className="mb-3">
                	<label htmlFor="pxp-company-about" className="form-label">Description de l'offre</label>
                        <textarea
						{...register('description')}	
						value={formData.description}
						onChange={handleDescriptionChange}
						style={{
							border:
								errors["description"] &&
								errors["description"].message
									? "1px solid rgba(255, 0, 0, 0.3)"
									: null, 
						}}
						placeholder={formData.description}
						className="form-control" 	id="pxp-company-about"></textarea>
						{errors && errors.description && <p className="text-danger">{errors.description.message}</p>}
                    </div>
                    <div className="mt-4 mt-lg-5">
                        <button className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
                    </div>
				</form>
			</div>
			</div>
		</>
	);
}
