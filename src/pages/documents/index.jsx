import React, { useState } from "react";
import Card from "./Card";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { InputView } from "../Dashboard/viewerPdf";

const Docs = [
	{
		name: "1 Wselect offre de service",
		path: "1_wselect_offre_de_service.pdf",
	},
	{ name: "2 Wselect About Us", path: "2_wselect_about_us.pdf" },
	{
		name: "3 Wselect Recrutement-Expert",
		path: "3_wselect _recrutement_expert.pdf",
	},
	{
		name: "4 Wselect notre processus de recrutement",
		path: "4_wselect_notre_processus_de_recrutement.pdf",
	},
	{
		name: "5 Wselect Doc Programme TET",
		path: "5_wselect_doc_programme_TET.pdf",
	},
	{
		name: "6 Wselect Devis profil bas salaire",
		path: "6_wselect_devis_profil_bas_salaire.pdf",
	},
	{
		name: "7 Wselect Devis profil haut salaire",
		path: "7_wselect_devis_profil_haut_salaire.pdf",
	},
	{ name: "8 Contrat Wselect", path: "8_contrat_wselect.pdf" },
];

function Documents() {
	const [file, setFile] = useState(null);
	// const [cvPath] = useState(profile?.cvPath || null);

	return (
		<>
			<div className="pxp-dashboard-content-details">
				<h1>Document Wselect</h1>
				<div
					style={{
						minHeight: "80vh",
					}}
				>
					{Docs.map((el, i) => (
						<Card key={i} name={el.name} path={el.path} />
					))}
				</div>
				<h1>Votre Contract</h1>
				{/* <InputView setFile={setFile} id="cv_file" cv={cvPath} /> */}
			</div>

			<footer>
				<div className="pxp-footer-copyright pxp-text-light">
					© {new Date().getFullYear()} World select. All Right
					Reserved.
				</div>
			</footer>
		</>
	);
}

export default Documents;
