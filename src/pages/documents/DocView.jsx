import React, { useState } from "react";

export default function DocView({ setFile, name, fileDoc, file }) {
	const [url, setUrl] = useState("");

	const onChange = (e) => {
		const files = e.target.files;
		setFile(e.target.files[0]);
		files.length > 0 && setUrl(URL.createObjectURL(files[0]));
	};

	if (fileDoc)
		return (
			<div>
				<div className="form-label">&nbsp;</div>
				<div className="pxp-candidate-cover mb-3">
					<a
						href={`${process.env.REACT_APP_API_STATIC}/static/${fileDoc}`}
						target="_blank"
						rel="noreferrer"
					>
						<div
							value={name}
							id="pxp-candidate-cover-choose-file"
						/>
						<label
							htmlFor="pxp-candidate-cover-choose-file"
							className="pxp-cover"
						>
							<span>Telecharger le Contrat</span>
						</label>
					</a>
				</div>
			</div>
		);

	return (
		<div>
			<div className="form-label">&nbsp;</div>
			<div className="pxp-candidate-cover mb-3">
				<input
					name={name}
					type="file"
					id="pxp-candidate-cover-choose-file"
					accept="application/pdf"
					onChange={onChange}
				/>
				<label
					htmlFor="pxp-candidate-cover-choose-file"
					className="pxp-cover"
				>
					<span>{url ? file?.name : "Envoyer votre contrat"}</span>
				</label>
			</div>
		</div>
	);
}
