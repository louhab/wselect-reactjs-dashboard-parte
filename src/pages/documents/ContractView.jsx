import React from "react";
import DocView from "./DocView";
import { instance } from "../../lib/axios";
import toast from "react-hot-toast";

export default function ContractView({ employer }) {
	const [file, setFile] = React.useState(null);
	const [contract] = React.useState(employer?.contract || null);

	function handleSubmit(e) {
		e.preventDefault();

		if (file) {
			const formData = new FormData();
			formData.append("contract", file);
			instance
				.put(`/employers/contract/${employer?.id}`, formData)
				.then(async (res) => {
					toast.success("Bravo!");
				})
				.catch(async (err) => {
					toast.error(err.message, {
						style: {
							borderRadius: "10px",
							background: "#333",
							color: "#fff",
							fontSize: "14px",
						},
					});
				});
		} else {
			toast.error("Nothing to upload !", {
				style: {
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
					fontSize: "14px",
				},
			});
		}
	}

	return (
		<div className="row mt-4 mt-lg-5">
			<h2>Contract d'employeur :</h2>
			<form onSubmit={handleSubmit}>
				<DocView
					setFile={setFile}
					file={file}
					id="contract"
					fileDoc={contract}
				/>
				<div
					style={{
						display: "flex",
						justifyContent: "flex-end",
					}}
				>
					{!employer?.contract && (
						<button
							className="btn rounded-pill pxp-section-cta"
							disabled={!file}
						>
							Envoyer
						</button>
					)}
				</div>
			</form>
		</div>
	);
}
