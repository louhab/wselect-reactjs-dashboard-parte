import React from "react";

export default function Card({ name = "Doc Name", path = "#" }) {
	return (
		<a
			href={`${process.env.REACT_APP_API_STATIC}/static/docs/${path}`}
			target="_blank"
			rel="noreferrer"
		>
			<div
				style={{
					backgroundColor: "#fff",
					boxShadow: "rgb(13 38 76 / 17%) 0px 2px 4px",
					padding: "15px 10px",
					borderRadius: "5px",
					display: "flex",
					justifyContent: "space-between",
					margin: "15px 0",
				}}
			>
				<div
					style={{
						fontWeight: "400",
					}}
				>
					{name}
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "end",
						width: "50%",
					}}
				>
					<div>
						<i
							className="fa fa-external-link fa-lg"
							aria-hidden="true"
						></i>
					</div>
				</div>
			</div>
		</a>
	);
}
