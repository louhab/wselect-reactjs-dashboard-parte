import React, { Suspense, useContext } from "react";
import Logo from "../assets/images/logo-transparant.png";
import PreLoader from "../components/PreLoader";
import { AuthContext } from "../context/Auth";
import { redirect } from "react-router-dom";

const MinimalHeader = React.lazy(() =>
	import("../components/layout/MinimalHeader")
);
const CandidateLoginForm = React.lazy(() =>
	import("../components/forms/CandidateLoginForm")
);

export default function UserSignIn() {
	const { auth } = useContext(AuthContext);

	React.useEffect(() => {
		if (auth) redirect("/");
	}, [auth]);

	return (
		<>
			<Suspense fallback={<PreLoader />}>
				<MinimalHeader
					logo={Logo}
					header="pxp-header fixed-top pxp-no-bg pxp-has-border"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex pxp-on-light"
				/>
				<CandidateLoginForm />
			</Suspense>
		</>
	);
}
