import React, { useEffect, useState, useContext } from "react";
import Content from "../../components/layout/Dashboard/Content";
import SidePanel from "../../components/layout/Dashboard/SidePanel";
import CandidateAnnoncesList from "../../pages/Dashboard/CandidateAnnoncesList";
import NavBar from "../../components/layout/Dashboard/navBar";
import { instance } from "../../lib/axios";
import { AuthContext } from "../../context/Auth";
import { Outlet } from "react-router-dom";

const CondidateDetails = () => {
	const { auth, id } = useContext(AuthContext);
	const user = auth?.user;
	const [position, setPosition] = useState(1);
	const [profile, setProfile] = useState(user);
	const [show, setShow] = useState(0);

	useEffect(() => {
		instance
			.get(`/candidates/${id}`)
			.then((res) => {
				const data = res?.data?.results;
				setProfile({});
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	const panelCandidate = [
		{ spanClass: "fa fa-home", title: "Dashboard", status: 1 },
		{ spanClass: "fa fa-lock", title: "Utilisateurs", status: 1 },
		{ spanClass: "fa fa-pencil", title: "Employeurs", status: 1 },
		{ spanClass: "fa fa-home", title: "Annonces", status: 1 },
	];

	return (
		<div
			style={{
				position: "relative",
				backgroundColor: "#e6f0f9",
				display: "flex",
				flexDirection: "row",
				width: "100%",
			}}
		>
			<SidePanel
				position={position}
				setPosition={setPosition}
				panel={panelCandidate}
				setShow={setShow}
				show={show}
			/>
			<div
				style={{
					position: "relative",
					padding: "20px 20px",
					width: "100%",
				}}
			>
				<NavBar
					setShow={setShow}
					candidate={`${profile.companyName}`}
				/>

				<Outlet />

				{/* {position === 0 ? (
					<Content />
				) : position === 1 ? (
					<CandidateAnnoncesList data={{ profile, id }} />
				) : position === 2 ? (
					<CandidateAnnoncesList data={{ profile, id }} />
				) : position === 3 ? (
					<Password data={{ id }} />
				) : position === 20 ? (
					<Inbox />
				) : (
					<>ok</>
				)} */}
			</div>
		</div>
	);
};

export default CondidateDetails;
