import React, { useEffect, useState } from "react";
import {
	Select,
	TextInput,
	Textarea,
} from "../../../components/inputDash/index";
import { instance } from "../../../lib/axios";
import { useParams } from "react-router-dom";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

export default function CreateOffer({ data }) {
	const { id } = useParams();

	const [employer, setEmployer] = useState({});

	const [carrerLevel, setCarrerLevel] = useState([]);

	const [employmentType, setEmploymentType] = useState([]);
	const [salaryRange, setSalaryRange] = useState([]);
	const [jobFields, setJobFields] = useState([]);
	const [countries, setCountries] = useState([]);
	const [cities, setCities] = useState([]);

	const years = [
		{ name: "1 an", id: "one" },
		{ name: "2 ans", id: "two" },
		{ name: "3 ans", id: "three" },
		{ name: "4 ans", id: "four" },
		{ name: "5 ans", id: "five" },
		{ name: "6 ans", id: "six" },
		{ name: "7 ans", id: "seven" },
		{ name: "8 ans", id: "eight" },
		{ name: "9 ans", id: "nine" },
		{ name: "10 ans", id: "ten" },
	];

	const schema = Joi.object({
		name: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		description: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		yearsExp: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		carrerLevelId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		employmentTypeId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		salaryRangeId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		jobFieldId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		employerId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		countryId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		cityId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
	});

	const {
		reset,
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (values) => {
		instance
			.post(`/annonces`, {
				...values,
				carrerLevelId: Number(values?.carrerLevelId),
				employmentTypeId: Number(values?.employmentTypeId),
				salaryRangeId: Number(values?.salaryRangeId),
				jobFieldId: Number(values?.jobFieldId),
				employerId: Number(id),
				countryId: Number(values?.countryId),
				cityId: Number(values?.cityId),
			})
			.then(async (res) => {
				reset();
				await Eggy({
					title: "Success",
					message: "offer created !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "offer doesn't created !",
					type: "error",
				});
				console.log(err);
			});
	};

	useEffect(() => {
		instance
			.get(`/carrer-levels`)
			.then((res) => {
				const data = res?.data?.results;
				setCarrerLevel(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/employer-types`)
			.then((res) => {
				const data = res?.data?.results;
				setEmploymentType(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/salary-ranges`)
			.then((res) => {
				const data = res?.data?.results;
				setSalaryRange(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/job-fields`)
			.then((res) => {
				const data = res?.data?.results;
				setJobFields(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/countries`)
			.then((res) => {
				const data = res?.data?.results;
				setCountries(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/cites`)
			.then((res) => {
				const data = res?.data?.results;
				setCities(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	React.useEffect(() => {
		instance
			.get(`/employers/${id}`)
			.then((res) => {
				const data = res?.data?.results;
				setEmployer(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [id]);

	// like is loading !!
	if (!employer?.id || !cities.length || !countries.length)
		return <div className="pxp-dashboard-content-details"></div>;

	return (
		<div className="pxp-dashboard-content-details">
			<h1>
				Nouvelle offre d'emploi pour{" "}
				<i
					style={{
						color: "#288dcd",
					}}
				>
					{employer?.companyName}
				</i>
			</h1>
			<p className="pxp-text-light">
				Ajoutez un nouveau poste à la liste des postes.
			</p>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="row">
						<TextInput
							name="name"
							label="Titre d'emploi"
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name="cityId"
								label="Ville"
								options={cities}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name="countryId"
								label="Pays"
								options={countries}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<Select
							name="jobFieldId"
							label="Domaine d'activité"
							options={jobFields}
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<Textarea
							name={"description"}
							label={"Description de l'emploi"}
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name={"yearsExp"}
								label={"Expérience"}
								options={years}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name={"carrerLevelId"}
								label={"Niveau de carrière"}
								options={carrerLevel}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name={"employmentTypeId"}
								label={"Type d'emploi"}
								options={employmentType}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name={"salaryRangeId"}
								label={"Échelle salariale"}
								options={salaryRange}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>
				<div className="mt-4 mt-lg-5">
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
}
