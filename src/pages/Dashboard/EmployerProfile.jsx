import React from "react";
import {
	TextInput,
	Select,
	RadioInput,
} from "../../components/inputDash/index";
import { instance } from "../../lib/axios";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { Link, useParams } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { ReactComponent as PlusIcon } from "../../assets/icons/plus.svg";
import Card from "../documents/Card";
import ContractView from "../documents/ContractView";
import dayjs from "dayjs";

const Docs = [
	{
		name: "1 Wselect offre de service",
		path: "1_wselect_offre_de_service.pdf",
	},
	{ name: "2 Wselect About Us", path: "2_wselect_about_us.pdf" },
	{
		name: "3 Wselect Recrutement-Expert",
		path: "3_wselect _recrutement_expert.pdf",
	},
	{
		name: "4 Wselect notre processus de recrutement",
		path: "4_wselect_notre_processus_de_recrutement.pdf",
	},
	{
		name: "5 Wselect Doc Programme TET",
		path: "5_wselect_doc_programme_TET.pdf",
	},
	{
		name: "6 Wselect Devis profil bas salaire",
		path: "6_wselect_devis_profil_bas_salaire.pdf",
	},
	{
		name: "7 Wselect Devis profil haut salaire",
		path: "7_wselect_devis_profil_haut_salaire.pdf",
	},
	{ name: "8 Contrat Wselect", path: "8_contrat_wselect.pdf" },
];

const Status = [
	{ name: "n/d", id: "Nd" },
	{ name: "Pas intéressé", id: "NotInterested" },
	{ name: "Contract", id: "Contract" },
	{ name: "Devis", id: "Devis" },
	{ name: "Injoignable", id: "Unreachable" },
	{ name: "Client", id: "Client" },
];

const IsActiveRadioGroup = [
	{ id: uuidv4(), label: "Active", value: true },
	{ id: uuidv4(), label: "Inactive", value: false },
];

export default function EmployerProfile() {
	const { id } = useParams();
	const [employer, setEmployer] = React.useState({});

	const [comments, setComments] = React.useState([]);
	const [commentInput, setCommentInput] = React.useState("");

	const handleCommentInput = (e) => {
		setCommentInput(e.target.value);
	};

	const handleAddComment = async () => {
		if (commentInput.trim() !== "") {
			setCommentInput("");
			const {
				data: { results },
			} = await instance.post(`/comments`, {
				employerId: id,
				comment: commentInput,
			});
			setComments([...comments, results]);
		}
	};

	const schema = Joi.object({
		companyName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		contactName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		contactPhone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		email: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		phone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		address: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		isActive: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),

		status: Joi.any()
			.valid(
				"Nd",
				"NotInterested",
				"Contract",
				"Devis",
				"Unreachable",
				"Client"
			)
			.messages({
				"any.only":
					"{{#label}} failed custom validation because {{#error.message}}",
			}),
	});

	const {
		register,
		handleSubmit,
		control,
		formState: { errors, isDirty, isSubmitting },
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (values) => {
		setEmployer((initial) => ({
			...initial,
			companyName: values?.companyName,
		}));
		instance
			.put(`/employers/${id}`, {
				...values,
			})
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "profile updated !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "profile doesn't update !",
					type: "error",
				});
				console.log(err);
			});
	};

	React.useEffect(() => {
		instance
			.get(`/employers/${id}`)
			.then((res) => {
				const data = res?.data?.results;
				setEmployer(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [id]);

	React.useEffect(() => {
		const fetchComments = async () => {
			try {
				const {
					data: { results },
				} = await instance.get(`/comments/${id}`);
				setComments(results);
			} catch ({
				response: {
					data: { error },
				},
			}) {
				console.log(error);
			}
		};
		fetchComments();
	}, [id]);

	// like is loading !!
	if (!employer?.id)
		return <div className="pxp-dashboard-content-details"></div>;

	return (
		<div className="pxp-dashboard-content-details">
			<h1>{employer?.companyName}</h1>
			<p className="pxp-text-light">
				Modifiez les informations de votre page de profile d'employer.
			</p>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<h2>Profile d'emplyeur :</h2>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="companyName"
								label="Nom de l'entreprise"
								defaultValue={employer?.companyName}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="contactName"
								label="Nom"
								defaultValue={employer?.contactName}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="email"
								label="Email"
								defaultValue={employer?.email}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="contactPhone"
								label="Phone"
								defaultValue={employer?.contactPhone}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name={"status"}
								label={"Status"}
								options={Status}
								register={register}
								errors={errors}
								defaultValue={employer?.status}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="address"
								label="Address"
								defaultValue={employer?.address}
								register={register}
								errors={errors}
							/>
						</div>
					</div>

					<div className="mb-3">
						<label className="form-label">Status de compte :</label>
						<br />
						<RadioInput
							control={control}
							name={"isActive"}
							radioGroup={IsActiveRadioGroup}
							defaultValue={employer?.isActive}
						/>
					</div>
				</div>
				<div
					style={{
						display: "flex",
						justifyContent: "flex-end",
					}}
				>
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
						disabled={!isDirty || isSubmitting}
					>
						Enregistrer
					</button>
				</div>
			</form>
			<div className="row mt-4 mt-lg-5">
				<h2>Fomulaires Wselect :</h2>
				<div className="row pt-2">
					<div className="col-sm-3">
						<Link
							to={`/employers/${id}/evaluation`}
							className="btn rounded-pill"
						>
							<PlusIcon
								style={{
									height: "18px",
									width: "18px",
								}}
							/>
							Demande d'évaluation
						</Link>
					</div>
					<div className="col-sm-3">
						<Link to={`/employers/${id}/new-offer`} className="btn">
							<PlusIcon
								style={{
									height: "18px",
									width: "18px",
								}}
							/>
							Nouvelle offre d'emploi
						</Link>
					</div>
				</div>
			</div>

			<div className="row mt-4 mt-lg-5">
				<h2>Documents Wselect :</h2>
				<div className="row pt-2">
					{Docs.map((el, i) => (
						<div key={i} className="col-sm-4">
							<Card name={el.name} path={el.path} />
						</div>
					))}
				</div>
			</div>

			<ContractView employer={employer} />

			<div className="row mt-4 mt-lg-5">
				<h2>Notes d'employeur :</h2>
				<div className="row pt-2">
					<div className="pxp-dashboard-inbox-messages-container">
						<div className="pxp-dashboard-inbox-messages-content">
							<div className="pxp-dashboard-inbox-messages-item">
								{comments?.map((el, i) => (
									<div
										key={i}
										className="row justify-content-end"
									>
										<div className="col-7 pt-4">
											<div className="pxp-dashboard-inbox-messages-item-header flex-row-reverse">
												<div className="pxp-dashboard-inbox-messages-item-name me-2">
													{el?.user?.fullname}
												</div>
												<div className="pxp-dashboard-inbox-messages-item-time pxp-text-light me-3">
													{dayjs(
														el?.createdAt
													).format(
														"DD-MM-YYYY HH:mm"
													)}
												</div>
											</div>
											<div className="pxp-dashboard-inbox-messages-item-message pxp-is-other mt-2">
												{/* {JSON.stringify(el)} */}
												{el.comment}
											</div>
										</div>
									</div>
								))}
							</div>
						</div>

						<div className="pxp-dashboard-inbox-messages-footer">
							<div className="pxp-dashboard-inbox-messages-footer-field">
								<input
									type="text"
									className="form-control"
									placeholder="Ajouter une Note"
									value={commentInput}
									onChange={handleCommentInput}
								/>
							</div>
							<div className="pxp-dashboard-inbox-messages-footer-btn">
								<button
									className="btn rounded-pill pxp-section-cta"
									onClick={handleAddComment}
								>
									Ajouter
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
