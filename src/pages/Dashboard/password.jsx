import React from "react";
import { TextInput } from "../../components/inputDash/index";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { instance } from "../../lib/axios";

const Password = ({ data }) => {
	const schema = Joi.object({
		currentPassword: Joi.string().required().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		newPassword: Joi.string().required().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		confirmNewPassword: Joi.any()
			.valid(Joi.ref("newPassword"))
			.required()
			.messages({
				"any.only": "doit correspondre au nouveau mot de passe",
			}),
	});
	const { id, user } = data;
	const {
		register,
		handleSubmit,
		formState: { errors },
		reset,
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (values) => {
		delete values.confirmNewPassword;
		instance
			.put(`${user}/updatePassword/${id}`, {
				...values,
			})
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "profile updated !",
					type: "success",
				});
				reset();
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "profile doesn't update !",
					type: "error",
				});
				console.log(err);
			});
	};

	return (
		<div
			className="pxp-dashboard-content-details"
			style={{
				padding: "60px 60px 160px",
			}}
		>
			<h1>Changer le mot de passe</h1>
			<p className="pxp-text-light">
				Choisissez un nouveau mot de passe de compte.
			</p>

			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="currentPassword"
								label="Ancien mot de passe"
								type={"password"}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="newPassword"
								label={"Nouveau mot de passe"}
								type={"password"}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="confirmNewPassword"
								label="Répéter le nouveau mot de passe"
								type={"password"}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>
				<div className="mt-4 mt-lg-5">
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
};

export default Password;
