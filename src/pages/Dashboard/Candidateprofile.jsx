import React, { useEffect, useState } from "react";
import { Select, TextInput } from "../../components/inputDash/index";
import { instance } from "../../lib/axios";
import { LEVELS, GENDER, LEVELS_LANG } from "./statiqueList";
import { InputView } from "./viewerPdf";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

const Candidateprofile = ({ data }) => {
	const { profile, id } = data;
	const [diplomeList, setdiplomeList] = useState([]);
	const [domainDactivityList, setdomainDactivityList] = useState([]);
	const [file, setFile] = useState(null);
	const [cvPath] = useState(profile?.cvPath || null);

	const schema = Joi.object({
		firstName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		lastName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		email: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		phone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		gender: Joi.any().valid("man", "woman").messages({
			"any.only":
				"{{#label}} failed custom validation because {{#error.message}}",
		}),
		address: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		diplomasName: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		yearsExp: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		languageLevel: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		diplomeId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		domaineDactiviteId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
	});

	const {
		register,
		handleSubmit,
		formState: { errors, isDirty, isSubmitting },
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (data) => {
		instance
			.put(`/candidates/${id}`, {
				...data,
				diplomeId: Number(data?.diplomeId),
				domaineDactiviteId: Number(data?.domaineDactiviteId),
			})
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "profile updated !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "profile doesn't updated !",
					type: "error",
				});
				console.log(err);
			});

		if (file) {
			const formData = new FormData();
			formData.append("cv_file", file);
			instance
				.put(`/candidates/updateCv/${id}`, formData)
				.then(async (res) => {
					await Eggy({
						title: "Success",
						message: "cv added !",
						type: "success",
					});
				})
				.catch(async (err) => {
					await Eggy({
						title: "Error",
						message: "cv doesn't added !",
						type: "error",
					});
					console.log(err);
				});
		}
	};

	useEffect(() => {
		instance
			.get("/domaines")
			.then((res) => {
				setdomainDactivityList(res?.data?.results);
			})
			.catch((err) => {
				console.log(err);
			});

		instance
			.get("/diplomes")
			.then((res) => {
				setdiplomeList(res?.data?.results);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<div className="pxp-dashboard-content-details">
			<h1>Editer le profil</h1>
			<p className="pxp-text-light">
				Modifiez les informations de votre page de profil de candidat.
			</p>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="firstName"
								label="Nom"
								defaultValue={profile?.firstName}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="lastName"
								label="Prénom"
								defaultValue={profile?.lastName}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="email"
								label="Email"
								type="email"
								defaultValue={profile?.email}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="phone"
								label="Numéro de téléphone"
								defaultValue={profile?.phone}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<Select
						name="gender"
						label="Genre"
						options={GENDER}
						defaultValue={profile?.gender}
						register={register}
						errors={errors}
					/>
					<TextInput
						name="address"
						label="Adresse"
						defaultValue={profile?.address}
						register={register}
						errors={errors}
					/>
					<TextInput
						name="diplomasName"
						label="Nom de Diplôme"
						defaultValue={profile?.diplomasName}
						register={register}
						errors={errors}
					/>

					<div className="row">
						<div className="col-sm-6">
							<Select
								name="yearsExp"
								label="Années D'expérience"
								options={LEVELS}
								defaultValue={profile?.yearsExp}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name="languageLevel"
								label="Niveau de Langue"
								options={LEVELS_LANG}
								defaultValue={profile?.languageLevel}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name="diplomeId"
								label="Diplome"
								options={diplomeList}
								defaultValue={profile?.diplomeId}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name="domaineDactiviteId"
								label="Domaine D'activite"
								options={domainDactivityList}
								defaultValue={profile?.domaineDactiviteId}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>
				<InputView setFile={setFile} id="cv_file" cv={cvPath} />
				<div className="mt-4 mt-lg-5">
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
						disabled={!isDirty || isSubmitting}
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
};

export default Candidateprofile;
