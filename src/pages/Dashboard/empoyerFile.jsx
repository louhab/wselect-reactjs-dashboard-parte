import React, { useEffect, useState, useContext } from "react";
import Content from "../../components/layout/Dashboard/Content";
import Documents from "../../pages/documents/index";
import SidePanel from "../../components/layout/Dashboard/SidePanel";
import EmployerPtofile from "../../pages/Dashboard/Employerprofile";
import ManageAnnoncesList from "./ManageAnnoncesList";
import { CreatOffer } from "../Dashboard/offerJob";
import Password from "../../pages/Dashboard/password";
import NavBar from "../../components/layout/Dashboard/navBar";
import { instance } from "../../lib/axios";
import { AuthContext } from "../../context/Auth";
import Inbox from "../../pages/inbox";
import { useLocation } from "react-router-dom";
import EvaluationRequest from "../Dashboard/employer/EvaluationRequest";

const EmployerDetails = ({ id }) => {
	const location = useLocation();
	const { auth } = useContext(AuthContext);
	const user = auth?.user;

	const [position, setPosition] = useState(location?.state === 20 ? 20 : 1);
	const [profile, setProfile] = useState(user);

	const employerId = id;

	useEffect(() => {
		instance
			.get(`/employers/${employerId}`)
			.then((res) => {
				const data = res?.data?.results;
				setProfile({
					contactName: data?.contactName,
					companieId: data?.companieId,
					isActive: data?.isActive,
					contactPhone: data?.contactPhone,
					address: data?.address,
					email: data?.email,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}, [employerId]);

	const panelEmployer = [
		{ spanClass: "fa fa-home", title: "Tableau de bord", status: 1 },
		{ spanClass: "fa fa-folder", title: "Documents", status: 1 },
		{ spanClass: "fa fa-pencil", title: "Editer le profil", status: 1 },
		{
			spanClass: "fa fa-wpforms",
			title: "Demande d'évaluation",
			status: 1,
		},
		{
			spanClass: "fa fa-file-text-o",
			title: "Nouvelle offre d'emploi",
			status: profile?.isActive ? 1 : 0,
		},
		{
			spanClass: "fa fa-lock",
			title: "Changer le mot de passe",
			status: 1,
		},
		{
			spanClass: "fa fa-home",
			title: "Annonces",
			status: profile?.isActive ? 1 : 0,
		},
	];

	return (
		<div
			style={{
				position: "relative",
				backgroundColor: "#e6f0f9",
				display: "flex",
				flexDirection: "row",
				width: "100%",
			}}
		>
			<SidePanel
				position={position}
				setPosition={setPosition}
				panel={panelEmployer}
			/>
			<div
				style={{
					position: "relative",
					padding: "20px 20px",
					width: "100%",
				}}
			>
				<NavBar />
				{position === 0 ? (
					<Content />
				) : position === 1 ? (
					<Documents />
				) : position === 2 ? (
					<EmployerPtofile
						data={{ profile: profile, id: employerId }}
					/>
				) : position === 4 ? (
					<CreatOffer
						data={{
							id: employerId,
							companieId: profile?.companieId,
						}}
					/>
				) : position === 3 ? (
					<EvaluationRequest />
				) : position === 5 ? (
					<Password data={{ id: employerId, user: "employers" }} />
				) : position === 6 ? (
					<ManageAnnoncesList data={{ id: employerId }} />
				) : position === 20 ? (
					<Inbox />
				) : (
					<>ok</>
				)}
			</div>
		</div>
	);
};

export default EmployerDetails;
