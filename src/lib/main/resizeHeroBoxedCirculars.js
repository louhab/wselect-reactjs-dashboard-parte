import $ from "jquery";

export default function resizeHeroBoxedCirculars() {
	if ($(".pxp-hero-boxed-circulars").length > 0) {
		const circularsWidth = $(".pxp-hero-boxed-circulars").width();
		$(".pxp-hero-boxed-circulars").height(circularsWidth);
	}
}
