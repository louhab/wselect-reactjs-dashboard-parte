import React, { useEffect } from "react";
import * as bootstrap from "bootstrap/dist/js/bootstrap.bundle.min";

const CustomDropdown = () => {
	useEffect(() => {
		(function () {
			const CLASS_NAME = "has-child-dropdown-show";

			document.querySelectorAll(".dropdown").forEach(function (dd) {
				dd.addEventListener("hide.bs.dropdown", function (e) {
					if (this.classList.contains(CLASS_NAME)) {
						this.classList.remove(CLASS_NAME);
						e.preventDefault();
					}
					e.stopPropagation();
				});
			});

			document
				.querySelectorAll(
					".dropdown-hover, .dropdown-hover-all .dropdown"
				)
				.forEach(function (dd) {
					console.log("working");
					dd.addEventListener("mouseenter", function (e) {
						let toggle = e.target.querySelector(
							':scope>[data-bs-toggle="dropdown"]'
						);
						if (!toggle.classList.contains("show")) {
							bootstrap.Dropdown.getOrCreateInstance(
								toggle
							).toggle();
							dd.classList.add(CLASS_NAME);
							bootstrap.Dropdown.clearMenus();
						}
					});
					dd.addEventListener("mouseleave", function (e) {
						let toggle = e.target.querySelector(
							':scope>[data-bs-toggle="dropdown"]'
						);
						if (toggle.classList.contains("show")) {
							bootstrap.Dropdown.getOrCreateInstance(
								toggle
							).toggle();
						}
					});
				});
		})();
	}, []);

	return <div></div>;
};

export default CustomDropdown;
