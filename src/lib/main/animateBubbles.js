import $ from "jquery";

export default function animateBubbles(element) {
	if ($(element).hasClass("pxp-animate-bounce")) {
		setTimeout(function () {
			$(element).addClass("animate__animated animate__bounceIn");
		}, 500);
	}
}
