import resizeHeroBoxedCirculars from "./resizeHeroBoxedCirculars";

export default function windowResizeHandler() {
	resizeHeroBoxedCirculars();
}
