import React from "react";

import ReactDOM from "react-dom/client";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";
import "./assets/css/font-awesome.min.css";
import "animate.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import UserSignIn from "./pages/UserSignIn";
import AuthProvider, { RequireAuth,ProtectedRoute } from "./context/Auth";
import DashLayout from "./layouts/DashLayout";
import Content from "./components/layout/Dashboard/Content";
import Index from "./components/layout/Dashboard/Index";
import EmployerProfile from "./pages/Dashboard/EmployerProfile";
import CustomDropdown from "./lib/main/customDropdown";
import CreateOffer from "./pages/Dashboard/employer/CreateOffer";
import CadidatesListe from "./pages/Candidates/CadidatesListe"
import CompaniesListe from './pages/Companies/CompaniesListe'
import OffersList from "./pages/Offers/OffersList";
import UsersList from "./pages/users/usersList";
import AbonnementsListe from './pages/abonnement/AbonnementsListe'
import AddOffer from './pages/Offers/AddOffer'
import AddUsers from './pages/users/AddUsers'
import CompanyDetails from "./pages/Companies/CompanyDetails";
import DetailsCandidate from "./pages/Candidates/DetailsCandidate";
import OfferDetails from './pages/Offers/OfferDetails'
import CandidatesOffer from './pages/Candidates/CandidatesOffer';
import EvaluationRequest from './pages/Dashboard/employer/EvaluationRequest'
import EditUser from './pages/users/EditUser'
import EditOffer from "./pages/Offers/EditOffer";
import EditProfile from "./pages/users/EditProfile";
import EditCompany from "./pages/Companies/EditCompany";


const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<RootEl />);
function RootEl() {
	return (
		<>
			<BrowserRouter>
				<AuthProvider>
					<CustomDropdown />
					<Routes>
						<Route element={<RequireAuth />}>
							<Route path="/" element={<DashLayout />}>
								<Route path="" element={<Index />} />
								<Route path="/companies" element={<CompaniesListe/>} />
								<Route path="/candidates" element={<CadidatesListe/>} />
								<Route path="/offers" element={<OffersList/>} />
								<Route path="/edit-profile/:id" element={<EditProfile/>} />
								<Route path="/abonnements" element={<AbonnementsListe/>} />
								<Route path="/edit-company/:id" element={<EditCompany/>} />
								<Route path="/add-offre" element={<AddOffer />}/>
								<Route path="/add-users" element={<AddUsers />}/>
								<Route path="/edit-users/:id" element={<EditUser />}/>
								<Route path="/company-details/:id" element={<CompanyDetails/>} />
								<Route path="/candidate-details/:id" element={<DetailsCandidate/>} />
								<Route path="/offer-details/:id" element={<OfferDetails/>} />
								<Route path="/offer-edit/:id" element={<EditOffer/>} />
								<Route path="/candidates-offer/:id" element={<CandidatesOffer/>} />
								<Route path="/evaluation-request" element={<EvaluationRequest/>} />
								<Route path="/employers/:id/new-offer" element={<CreateOffer />}/>
								<Route path="/employers/:id" element={<EmployerProfile />}/>
								<Route path="*" element={<Content />} />
							</Route>
						</Route>
						<Route element={<ProtectedRoute/>}>
							<Route path="/" element={<DashLayout />}>
							<Route path="/abonnements" element={<AbonnementsListe/>} />
							<Route path="/users" element={<UsersList/>} />
							<Route path="*" element={<Content />} />
						</Route>
						</Route> 
						<Route path="/auth/login" element={<UserSignIn />} />
						<Route path="*" element={<DashLayout />} />
					</Routes>
				</AuthProvider>
			</BrowserRouter>
		</>
	);
}
