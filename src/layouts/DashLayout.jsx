import React, { useEffect, useState, useContext } from "react";
import SidePanel from "../components/layout/Dashboard/SidePanel";
import NavBar from "../components/layout/Dashboard/navBar";
import { instance } from "../lib/axios";
import { AuthContext } from "../context/Auth";
import { Outlet } from "react-router-dom";

const DashLayout = () => {
	const { auth } = useContext(AuthContext);
	const [position, setPosition] = useState(1);
	const [profile, setProfile] = useState({});
	const [show, setShow] = useState(0);

	useEffect(() => {
		instance
			.get(`/users/${auth?.id}`)
			.then((res) => {
				const data = res?.data?.results;
				setProfile({
					...data,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}, [auth]);

	const panelCandidate = [
		{id: 1 , spanClass: "fa fa-home", title: "Dashboard", status: 1, path: "/" },
		{id: 2 , spanClass: "fa fa-user-circle", title: "Entreprises", status: 1, path: "/companies" },
		{id: 3 ,  spanClass: "fa fa-user-circle", title: "Candidates", status: 1, path: "/candidates" },
		{id: 4 , spanClass: "fa fa-suitcase", title: "Offers", status: 1, path: "/offers" },

		//{
		//	spanClass: "fa fa-lock",
		//	title: "Utilisateurs",
		//	status: 1,
		//	path: "/users",
		//},
		//{
		//	spanClass: "fa fa-pencil",
		//	title: "Employeurs",
		//	status: 1,
		//	path: "/employers",
		//},
		//{
		//	spanClass: "fa fa-home",
		//	title: "Annonces",
		//	status: 1,
		//	path: "/test",
		//},
	];

	return (
		<div
			style={{
				backgroundColor: "#E4F0FA",
				display: "flex",
				flexDirection: "row",
				height: "100%",
			}}
		>
			<div
				style={{
					position: "relative",
					// backgroundColor: "#fff8ec",
					display: "flex",
					flexDirection: "row",
					width: "100%",
				}}
			>
				<SidePanel
					position={position}
					setPosition={setPosition}
					panel={panelCandidate}
					setShow={setShow}
					show={show}
				/>
				<div
					style={{
						position: "relative",
						padding: "20px 20px",
						width: "100%",
					}}
				>
					<NavBar
						setShow={setShow}
						candidate={`${profile?.fullname || ""}`}
					/>

					<Outlet />
				</div>
			</div>
		</div>
	);
};

export default DashLayout;
